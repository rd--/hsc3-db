hsc3-db
-------

[hsc3-db](http://rohandrape.net/?t=hsc3-db)
is a
[Haskell](http://haskell.org/)
library that contains a database of
[SuperCollider](http://audiosynth.com/)
unit generators.

The database is derived from the sclang run-time environment.  To
generate type:

    $ sclang scd/gen-db.scd - loc
    $ sclang scd/gen-db.scd - ext

The database has additional information about UGen structure encoded at
[DB.Meta](http://rohandrape.net/?t=hsc3-db&e=Sound/SC3/UGen/DB/Meta.hs).

hsc3-db is both partial and partially inaccurate, but is useful as a
starting point.

There are modules to generate:

- Haskell bindings,
  [DB.Bindings.Haskell](http://rohandrape.net/?t=hsc3-db&e=Sound/SC3/UGen/DB/Bindings/Haskell.hs)
- Scheme bindings,
  [DB.Bindings.Scheme](http://rohandrape.net/?t=hsc3-db&e=Sound/SC3/UGen/DB/Bindings/Scheme.hs)
- Smalltalk bindings,
  [DB.Bindings.Smalltalk](http://rohandrape.net/?t=hsc3-db&e=Sound/SC3/UGen/DB/Bindings/Smalltalk.hs)
- CommonLisp bindings,
  [DB.Bindings.CommonLisp](http://rohandrape.net/?t=hsc3-db&e=Sound/SC3/UGen/DB/Bindings/CommonLisp.hs)

hsc3-db generated bindings are used at:
[hsc3](http://rohandrape.net/?t=hsc3),
[rsc3](http://rohandrape.net/?t=rsc3),
[stsc3](http://rohandrape.net/?t=stsc3),
[smlsc3](http://rohandrape.net/?t=smlsc3),
[hsc3-forth](http://rohandrape.net/?t=hsc3-forth)
&
[hsc3-lisp](http://rohandrape.net/?t=hsc3-lisp)

initial announcement: [2014-10-15/sc-users](http://rohandrape.net/?t=hsc3-db&e=md/announce.text)

<!--
[[bham](http://www.listarc.bham.ac.uk/lists/sc-users/msg42058.html),
 [gmane](http://article.gmane.org/gmane.comp.audio.supercollider.user/110166)]
-->

## cli

[hsc3-db](http://rohandrape.net/?t=hsc3-db&e=md/db.md),
[hsc3-help](http://rohandrape.net/?t=hsc3-db&e=md/help.md)

© [rohan drape](http://rohandrape.net/), 2006-2024, [gpl](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 144  Tried: 144  Errors: 0  Failures: 0
$
```
