-- | Rather ad-hoc accumulation of meta-data about Sc3.Ugens.
module Sound.Sc3.Ugen.Db.Meta where

import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Safe {- safe -}

import Sound.Sc3.Common.Rate {- hsc3 -}

-- | Meta data not gleamed from sclang, or that may require correction.
data Meta = Meta
  { m_name :: String
  -- ^ SuperCollider name, set only for names the standard rules don't cover.
  , m_hs_name :: Maybe String
  -- ^ Haskell name, if not obtained by simply downcasing the initial letter.
  , m_nc_input :: Bool
  -- ^ Number of output channels is given as psuedo input.
  , m_nc_mce :: Maybe Int
  -- ^ Number of output channels is given by degree of MCE input plus indicated offset.
  , m_input_reorder :: Maybe [Int]
  -- ^ Sc3 reordering, pseudo (ie. NC) inputs at sclang ugens aren't mentioned here.
  -- Values indicate location of indexed input after move.
  , m_std_mce :: Int
  -- ^ UGens that have MCE inputs as the last /n/ inputs.
  , m_secondary_mce :: Maybe Int
  -- ^ Secondary, length pre-fixed MCE input (ie. dwrand, not working...).
  , m_enumerations :: Maybe [(Int, String)]
  -- ^ Enumerated (not plain) inputs.
  -- Indices are /UGen/ indices, not /sclang/ indices.
  , m_filter :: Maybe [Int]
  -- ^ Rate is given by rate of indicated inputs.
  -- Indices are /UGen/ indices, not /sclang/ indices.
  , m_fixed_rate :: Maybe Rate
  -- ^ Operate only at one fixed rate, which can be elided.
  -- There is a distinction between UGens that have only one rate implemented
  -- but could operate at other rates, and UGens that are inherently fixed rate.
  , m_nondet :: Bool
  -- ^ Required distinguishing identifier for (in-)equality.
  , m_pseudo_inputs :: Maybe [Int]
  -- ^ Sc3 binding includes a pseudo input (not NC).
  , m_i_rate :: Bool
  -- ^ Operates at i-rate despite not having .ir method.
  , m_n_outputs :: Maybe Int
  -- ^ Number of output channels.
  , m_unipolar :: Bool
  -- ^ Is signal range (0,1) and not (-1,1).
  }
  deriving (Show)

-- | Default (empty) meta data.
def_meta :: String -> Meta
def_meta sc3_nm =
  let n = Nothing
      f = False
      fx = if "PV_" `isPrefixOf` sc3_nm then Just kr else Nothing
  in Meta sc3_nm Nothing f n n 0 n n n fx f n f Nothing False

def_meta_nm :: String -> String -> Meta
def_meta_nm hs_nm sc3_nm = (def_meta sc3_nm) {m_hs_name = Just hs_nm}

{- | Function to apply re-ordering permutation (forwards) to an index.
     I.e. alias for erroring elemIndex function.

>>> map (reorder_index_forwards [3,0,1,2]) [0,1,2,3]
[1,2,3,0]
-}
reorder_index_forwards :: [Int] -> Int -> Int
reorder_index_forwards o i = fromMaybe (error "apply_reorder_forwards") (elemIndex i o)

{- | Function to apply re-ordering permutation (forwards).
     The permutation at each place tells the index that value is sent to.

>>> apply_reorder_forwards [3,0,1,2] "abcd"
"bcda"
-}
apply_reorder_forwards :: [Int] -> [t] -> [t]
apply_reorder_forwards o x =
  let f i = Safe.atNote "apply_reorder_forwards" x (reorder_index_forwards o i)
  in map f [0 .. length x - 1]

{- | Function to apply re-ordering permutation (backwards).
     The permutation at each place tells the index that value is fetched from.

>>> apply_reorder_backwards [3,0,1,2] "bcda"
"abcd"
-}
apply_reorder_backwards :: [Int] -> [t] -> [t]
apply_reorder_backwards o x =
  let f i = Safe.atNote "apply_reorder_backwards" x i
  in map f o

{- | Reordering input to move first input to the end (ie. [n-1,0,..,n-2]).

>>> reorder_first_last 4
Just [3,0,1,2]
-}
reorder_first_last :: (Num i, Enum i) => i -> Maybe [i]
reorder_first_last k = Just ((k - 1) : [0 .. k - 2])

{- | Reordering input to move last input to the start (ie. [1,..,n-1,0])

>>> reorder_last_first 4
Just [1,2,3,0]
-}
reorder_last_first :: (Num i, Enum i) => i -> Maybe [i]
reorder_last_first k = Just ([1 .. k - 1] ++ [0])

{- | Reordering to swap two inputs (ie. [1,0])

>>> reorder_swap
Just [1,0]
-}
reorder_swap :: Num i => Maybe [i]
reorder_swap = Just [1, 0]

-- | Encoding of 'Meta'.
meta_data_core :: [Meta]
meta_data_core =
  [ (def_meta "A2K") {m_fixed_rate = Just kr}
  , (def_meta "APF") {m_filter = Just [0]}
  , (def_meta "AllpassC") {m_filter = Just [0]}
  , (def_meta "AllpassL") {m_filter = Just [0]}
  , (def_meta "AllpassN") {m_filter = Just [0]}
  , (def_meta "Balance2") {m_filter = Just [0, 1]}
  , (def_meta "BAllPass") {m_filter = Just [0]}
  , (def_meta "BBandPass") {m_filter = Just [0]}
  , (def_meta "BBandStop") {m_filter = Just [0]}
  , (def_meta "BHiPass") {m_filter = Just [0]}
  , (def_meta "BHiShelf") {m_filter = Just [0]}
  , (def_meta "BlockSize") {m_fixed_rate = Just ir}
  , (def_meta "BLowPass") {m_filter = Just [0]}
  , (def_meta "BLowShelf") {m_filter = Just [0]}
  , (def_meta "BPF") {m_filter = Just [0]}
  , (def_meta "BPZ2") {m_filter = Just [0]}
  , (def_meta "BPeakEQ") {m_filter = Just [0]}
  , (def_meta "BRF") {m_filter = Just [0]}
  , (def_meta "BRZ2") {m_filter = Just [0]}
  , (def_meta "BinaryOpUGen") {m_filter = Just [0, 1]}
  , (def_meta "BrownNoise") {m_nondet = True}
  , (def_meta "BufAllpassC") {m_filter = Just [1]}
  , (def_meta "BufAllpassL") {m_filter = Just [1]}
  , (def_meta "BufAllpassN") {m_filter = Just [1]}
  , (def_meta "BufCombC") {m_filter = Just [1]}
  , (def_meta "BufCombL") {m_filter = Just [1]}
  , (def_meta "BufCombN") {m_filter = Just [1]}
  , (def_meta "BufDelayC") {m_filter = Just [1]}
  , (def_meta "BufDelayL") {m_filter = Just [1]}
  , (def_meta "BufDelayN") {m_filter = Just [1]}
  , (def_meta "BufRd") {m_nc_input = True, m_filter = Just [1], m_enumerations = Just [(2, "Loop"), (3, "Interpolation")]}
  , (def_meta "BufWr") {m_filter = Just [3], m_enumerations = Just [(2, "Loop")], m_std_mce = 1, m_input_reorder = reorder_first_last 4}
  , (def_meta "CheckBadValues") {m_filter = Just [0]}
  , (def_meta "Clip") {m_filter = Just [0]}
  , (def_meta "ClipNoise") {m_nondet = True}
  , (def_meta "CoinGate") {m_filter = Just [1], m_nondet = True}
  , (def_meta "CombC") {m_filter = Just [0]}
  , (def_meta "CombL") {m_filter = Just [0]}
  , (def_meta "CombN") {m_filter = Just [0]}
  , (def_meta "Compander") {m_filter = Just [0]}
  , (def_meta "ComplexRes") {m_filter = Just [0]}
  , (def_meta "ControlDur") {m_fixed_rate = Just ir}
  , (def_meta "ControlRate") {m_fixed_rate = Just ir}
  , (def_meta "Convolution") {m_fixed_rate = Just ar}
  , (def_meta "Convolution2") {m_fixed_rate = Just ar}
  , (def_meta "Dbrown") {m_input_reorder = reorder_last_first 4}
  , (def_meta "Dbufrd") {m_enumerations = Just [(2, "Loop")]}
  , (def_meta "Dbufwr") {m_enumerations = Just [(3, "Loop")], m_input_reorder = Just [2, 0, 1, 3]}
  , (def_meta "Decay") {m_filter = Just [0]}
  , (def_meta "Decay2") {m_filter = Just [0]}
  , (def_meta "DecodeB2") {m_filter = Just [0, 1, 2], m_nc_input = True}
  , (def_meta "DegreeToKey") {m_filter = Just [1]}
  , (def_meta "Delay1") {m_filter = Just [0]}
  , (def_meta "Delay2") {m_filter = Just [0]}
  , (def_meta "DelayC") {m_filter = Just [0]}
  , (def_meta "DelayL") {m_filter = Just [0]}
  , (def_meta "DelayN") {m_filter = Just [0]}
  , (def_meta "DelTapRd") {m_filter = Just [1]}
  , (def_meta "DelTapWr") {m_filter = Just [1]}
  , (def_meta "Demand") {m_filter = Just [0], m_std_mce = 1, m_nc_mce = Just 0}
  , (def_meta "DemandEnvGen") {m_enumerations = Just [(9, "DoneAction")]}
  , (def_meta "DetectIndex") {m_filter = Just [1]}
  , (def_meta "DetectSilence") {m_enumerations = Just [(3, "DoneAction")], m_filter = Just [0]}
  , (def_meta "Dgeom") {m_input_reorder = reorder_last_first 3}
  , (def_meta "Dibrown") {m_input_reorder = reorder_last_first 4}
  , (def_meta "DiodeRingMod") {m_filter = Just [0]}
  , (def_meta "DiskIn") {m_enumerations = Just [(1, "Loop")], m_nc_input = True, m_fixed_rate = Just ar}
  , (def_meta "DiskOut") {m_fixed_rate = Just ar, m_std_mce = 1}
  , (def_meta "Diwhite") {m_input_reorder = reorder_last_first 3}
  , (def_meta "Done") {m_fixed_rate = Just kr}
  , (def_meta "Drand") {m_std_mce = 1, m_input_reorder = reorder_swap}
  , (def_meta "Dseq") {m_std_mce = 1, m_input_reorder = reorder_swap}
  , (def_meta "Dser") {m_std_mce = 1, m_input_reorder = reorder_swap}
  , (def_meta "Dseries") {m_input_reorder = reorder_last_first 3}
  , (def_meta "Dshuf") {m_std_mce = 1, m_input_reorder = reorder_swap}
  , (def_meta "Dswitch") {m_std_mce = 1, m_input_reorder = reorder_swap}
  , (def_meta "Dswitch1") {m_std_mce = 1, m_input_reorder = reorder_swap}
  , (def_meta "Dust") {m_nondet = True, m_unipolar = True}
  , (def_meta "Dust2") {m_nondet = True}
  , (def_meta "Duty") {m_enumerations = Just [(2, "DoneAction")], m_input_reorder = Just [0, 1, 3, 2]}
  , (def_meta "Dwhite") {m_input_reorder = reorder_last_first 3}
  , (def_meta "Dwrand") {m_secondary_mce = Just 1, m_std_mce = 1, m_input_reorder = Just [2, 1, 0]}
  , (def_meta "Dxrand") {m_std_mce = 1, m_input_reorder = reorder_swap}
  , (def_meta "EnvGen") {m_enumerations = Just [(4, "DoneAction"), (5, "Envelope")], m_std_mce = 1, m_input_reorder = reorder_first_last 6}
  , (def_meta "ExpRand") {m_fixed_rate = Just ir, m_nondet = True}
  , (def_meta "FFT") {m_fixed_rate = Just kr}
  , (def_meta "FOS") {m_filter = Just [0]}
  , (def_meta "Fold") {m_filter = Just [0]}
  , (def_meta "Formlet") {m_filter = Just [0]}
  , (def_meta "Free") {m_filter = Just [0], m_fixed_rate = Just kr}
  , (def_meta "FreeSelf") {m_fixed_rate = Just kr}
  , (def_meta "FreeSelfWhenDone") {m_fixed_rate = Just kr}
  , (def_meta "FreeVerb") {m_filter = Just [0]}
  , (def_meta "FreeVerb2") {m_filter = Just [0]}
  , (def_meta "FreqShift") {m_fixed_rate = Just ar}
  , (def_meta "GVerb") {m_filter = Just [0]}
  , (def_meta "Gate") {m_filter = Just [0]}
  , (def_meta "Gendy1") {m_nondet = True}
  , (def_meta "Gendy2") {m_nondet = True}
  , (def_meta "Gendy3") {m_nondet = True}
  , (def_meta "GrainBuf") {m_fixed_rate = Just ar, m_nc_input = True}
  , (def_meta "GrainFM") {m_fixed_rate = Just ar, m_nc_input = True}
  , (def_meta "GrainIn") {m_fixed_rate = Just ar, m_nc_input = True}
  , (def_meta "GrainSin") {m_fixed_rate = Just ar, m_nc_input = True}
  , (def_meta "GrayNoise") {m_nondet = True}
  , (def_meta "Greyhole") {m_filter = Just [0]}
  , (def_meta "HPF") {m_filter = Just [0]}
  , (def_meta "HPZ1") {m_filter = Just [0]}
  , (def_meta "HPZ2") {m_filter = Just [0]}
  , (def_meta "Hasher") {m_filter = Just [0]}
  , (def_meta "Hilbert") {m_filter = Just [0]}
  , (def_meta "IEnvGen") {m_enumerations = Just [(1, "IEnvelope")], m_std_mce = 1, m_input_reorder = reorder_swap}
  , (def_meta "IFFT") {m_fixed_rate = Just ar}
  , (def_meta "Impulse") {m_unipolar = True}
  , (def_meta "IRand") {m_fixed_rate = Just ir, m_nondet = True}
  , (def_meta_nm "in'" "In") {m_nc_input = True}
  , (def_meta "InFeedback") {m_fixed_rate = Just ar, m_nc_input = True}
  , (def_meta "InRange") {m_filter = Just [0]}
  , (def_meta "InTrig") {m_nc_input = True, m_fixed_rate = Just kr}
  , (def_meta "Index") {m_filter = Just [1]}
  , (def_meta "IndexInBetween") {m_filter = Just [1]}
  , (def_meta "IndexL") {m_filter = Just [1]}
  , (def_meta "Integrator") {m_filter = Just [0]}
  , (def_meta "JPverbRaw") {m_filter = Just [0]}
  , (def_meta "K2A") {m_fixed_rate = Just ar}
  , (def_meta "Klang") {m_std_mce = 1, m_input_reorder = reorder_first_last 3}
  , (def_meta "Klank") {m_filter = Just [0], m_std_mce = 1, m_input_reorder = reorder_first_last 5}
  , (def_meta "LFClipNoise") {m_nondet = True}
  , (def_meta "LFDClipNoise") {m_nondet = True}
  , (def_meta "LFDNoise0") {m_nondet = True}
  , (def_meta "LFDNoise1") {m_nondet = True}
  , (def_meta "LFDNoise3") {m_nondet = True}
  , (def_meta "LFGauss") {m_enumerations = Just [(3, "Loop"), (4, "DoneAction")]}
  , (def_meta "LFNoise0") {m_nondet = True}
  , (def_meta "LFNoise1") {m_nondet = True}
  , (def_meta "LFNoise2") {m_nondet = True}
  , (def_meta "LFPulse") {m_unipolar = True}
  , (def_meta "LPF") {m_filter = Just [0]}
  , (def_meta "LPZ1") {m_filter = Just [0]}
  , (def_meta "LPZ2") {m_filter = Just [0]}
  , (def_meta "Lag") {m_filter = Just [0], m_n_outputs = Just 1}
  , (def_meta "Lag2") {m_filter = Just [0], m_n_outputs = Just 1}
  , (def_meta "Lag2UD") {m_filter = Just [0], m_n_outputs = Just 1}
  , (def_meta "Lag3") {m_filter = Just [0], m_n_outputs = Just 1}
  , (def_meta "Lag3UD") {m_filter = Just [0], m_n_outputs = Just 1}
  , (def_meta "LagIn") {m_nc_input = True, m_fixed_rate = Just kr}
  , (def_meta "LagUD") {m_filter = Just [0], m_n_outputs = Just 1}
  , (def_meta "LastValue") {m_filter = Just [0]}
  , (def_meta "Latch") {m_filter = Just [0]} -- [0, 1]
  , (def_meta "LeakDC") {m_filter = Just [0]}
  , (def_meta "Limiter") {m_filter = Just [0]}
  , (def_meta "LinExp") {m_filter = Just [0], m_i_rate = True}
  , (def_meta "LinPan2") {m_filter = Just [0]}
  , (def_meta "LinRand") {m_fixed_rate = Just ir, m_nondet = True}
  , (def_meta "LinXFade2") {m_filter = Just [0, 1], m_pseudo_inputs = Just [3]}
  , (def_meta "Line") {m_enumerations = Just [(3, "DoneAction")]}
  , (def_meta "Linen") {m_enumerations = Just [(4, "DoneAction")], m_fixed_rate = Just kr}
  , (def_meta "LocalBuf") {m_fixed_rate = Just ir, m_input_reorder = reorder_swap, m_nondet = True}
  , (def_meta "LocalIn") {m_nc_input = True, m_std_mce = 1}
  , (def_meta "LocalOut") {m_filter = Just [0], m_std_mce = 1, m_n_outputs = Just 0}
  , (def_meta "LoopBuf") {m_nc_input = True}
  , (def_meta "Loudness") {m_fixed_rate = Just kr}
  , (def_meta "MantissaMask") {m_filter = Just [0]}
  , (def_meta "MaxLocalBufs") {m_fixed_rate = Just kr}
  , (def_meta "Median") {m_filter = Just [1]}
  , (def_meta "MidEQ") {m_filter = Just [0]}
  , (def_meta "ModDif") {m_filter = Just [0]}
  , (def_meta "MoogFF") {m_filter = Just [0]}
  , (def_meta "MostChange") {m_filter = Just [0, 1]}
  , (def_meta "MouseButton") {m_unipolar = True} -- default range
  , (def_meta "MouseX") {m_enumerations = Just [(2, "Warp")], m_unipolar = True} -- default range
  , (def_meta "MouseY") {m_enumerations = Just [(2, "Warp")], m_unipolar = True} -- default range
  , (def_meta "MulAdd") {m_filter = Just [0, 1, 2]} -- the rules for MulAdd are complicated...
  , (def_meta "NRand") {m_fixed_rate = Just ir, m_nondet = True}
  , (def_meta "Normalizer") {m_filter = Just [0]}
  , (def_meta "NumAudioBuses") {m_fixed_rate = Just ir}
  , (def_meta "NumBuffers") {m_fixed_rate = Just ir}
  , (def_meta "NumControlBuses") {m_fixed_rate = Just ir}
  , (def_meta "NumInputBuses") {m_fixed_rate = Just ir}
  , (def_meta "NumOutputBuses") {m_fixed_rate = Just ir}
  , (def_meta "NumRunningSynths") {m_fixed_rate = Just ir}
  , (def_meta "OffsetOut") {m_filter = Just [1], m_std_mce = 1, m_n_outputs = Just 0}
  , (def_meta "OnePole") {m_filter = Just [0]}
  , (def_meta "OneZero") {m_filter = Just [0]}
  , (def_meta "Onsets") {m_fixed_rate = Just kr}
  , (def_meta "Out") {m_filter = Just [1], m_std_mce = 1, m_n_outputs = Just 0}
  , (def_meta "Pause") {m_fixed_rate = Just kr}
  , (def_meta "PauseSelf") {m_fixed_rate = Just kr}
  , (def_meta "PauseSelfWhenDone") {m_fixed_rate = Just kr}
  , (def_meta "PV_BinScramble") {m_nondet = True}
  , (def_meta "PV_RandComb") {m_nondet = True}
  , (def_meta "PV_RandWipe") {m_nondet = True}
  , (def_meta "PV_Split") {m_fixed_rate = Just kr}
  , (def_meta "PackFFT") {m_std_mce = 1, m_input_reorder = Just [0, 1, 6, 2, 3, 4]} -- 5: implicit
  , (def_meta "Pan2") {m_filter = Just [0]}
  , (def_meta "PanAz") {m_filter = Just [0], m_nc_input = True}
  , (def_meta "PanB2") {m_filter = Just [0]}
  , (def_meta "PartConv") {m_fixed_rate = Just ar}
  , (def_meta "Peak") {m_filter = Just [0]}
  , (def_meta "PeakFollower") {m_filter = Just [0]}
  , (def_meta "PinkNoise") {m_nondet = True}
  , (def_meta "Pitch") {m_fixed_rate = Just kr}
  , (def_meta "PitchShift") {m_filter = Just [0]}
  , (def_meta "PlayBuf") {m_enumerations = Just [(4, "Loop"), (5, "DoneAction")], m_nc_input = True}
  , (def_meta "Pluck") {m_filter = Just [0]}
  , (def_meta "Poll") {m_filter = Just [1], m_input_reorder = Just [0, 1, 3, 2]} -- m_enumerations = Just [(3,"Label")]
  , (def_meta "PulseCount") {m_filter = Just [0]}
  , (def_meta "PulseDivider") {m_filter = Just [0]}
  , (def_meta "RHPF") {m_filter = Just [0]}
  , (def_meta "RLPF") {m_filter = Just [0]}
  , (def_meta "RLPFD") {m_filter = Just [0]}
  , (def_meta "RadiansPerSample") {m_fixed_rate = Just ir}
  , (def_meta "Ramp") {m_filter = Just [0], m_n_outputs = Just 1}
  , (def_meta "Rand") {m_fixed_rate = Just ir, m_nondet = True}
  , (def_meta "RandID") {m_n_outputs = Just 0}
  , (def_meta "RandSeed") {m_n_outputs = Just 0}
  , (def_meta "RecordBuf") {m_enumerations = Just [(5, "Loop"), (7, "DoneAction")], m_std_mce = 1, m_input_reorder = reorder_first_last 9}
  , (def_meta "ReplaceOut") {m_filter = Just [1], m_std_mce = 1, m_n_outputs = Just 0}
  , (def_meta "Resonz") {m_filter = Just [0]}
  , (def_meta "Ringz") {m_filter = Just [0]}
  , (def_meta "Rotate2") {m_filter = Just [0, 1]}
  , (def_meta "RunningMax") {m_filter = Just [0]}
  , (def_meta "RunningMin") {m_filter = Just [0]}
  , (def_meta "RunningSum") {m_filter = Just [0]}
  , (def_meta "Sanitize") {m_filter = Just [0]}
  , (def_meta "SOS") {m_filter = Just [0]}
  , (def_meta "SampleDur") {m_fixed_rate = Just ir}
  , (def_meta "SampleRate") {m_fixed_rate = Just ir}
  , (def_meta "Schmidt") {m_filter = Just [0]}
  , (def_meta "Select") {m_filter = Just [0, 1], m_std_mce = 1, m_i_rate = True} -- [1]?
  , (def_meta "SendReply") {m_filter = Just [0]}
  , (def_meta "SendTrig") {m_filter = Just [0], m_n_outputs = Just 0}
  , (def_meta "SetBuf") {m_fixed_rate = Just ir, m_std_mce = 1, m_input_reorder = Just [0, 1, 2, 3]} -- local def. -- length implicit
  , (def_meta "SetResetFF") {m_filter = Just [0, 1]}
  , (def_meta "Shaper") {m_filter = Just [1]}
  , (def_meta "Slew") {m_filter = Just [0], m_n_outputs = Just 1} -- Slew checks for i-rate input and bypasses
  , (def_meta "Slope") {m_filter = Just [0]}
  , (def_meta "Stepper") {m_filter = Just [0]}
  , (def_meta "SubsampleOffset") {m_fixed_rate = Just ir}
  , (def_meta "Sum3") {m_filter = Just [0, 1, 2]}
  , (def_meta "Sum4") {m_filter = Just [0, 1, 2, 3]}
  , -- Sweep is not a filter
    (def_meta "T2K") {m_fixed_rate = Just kr}
  , (def_meta "T2A") {m_fixed_rate = Just ar}
  , (def_meta "TChoose") {m_nondet = True}
  , (def_meta "TDelay") {m_filter = Just [0]}
  , (def_meta "TDuty") {m_enumerations = Just [(2, "DoneAction")], m_input_reorder = Just [0, 1, 3, 2, 4]}
  , (def_meta "TExpRand") {m_filter = Just [2], m_nondet = True}
  , (def_meta "TGrains") {m_fixed_rate = Just ar, m_nc_input = True}
  , (def_meta "TGrains2") {m_nc_input = True}
  , (def_meta "TGrains3") {m_nc_input = True}
  , (def_meta "TIRand") {m_filter = Just [2], m_nondet = True}
  , (def_meta "TRand") {m_filter = Just [2], m_nondet = True}
  , (def_meta "TWChoose") {m_nondet = True}
  , (def_meta "TWindex") {m_filter = Just [0], m_nondet = True, m_std_mce = 1, m_input_reorder = Just [0, 2, 1]}
  , (def_meta "Tap") {m_nc_input = True}
  , (def_meta "Timer") {m_filter = Just [0]}
  , (def_meta "ToggleFF") {m_filter = Just [0]}
  , (def_meta "Trig") {m_filter = Just [0]}
  , (def_meta "Trig1") {m_filter = Just [0], m_unipolar = True}
  , (def_meta "TwoPole") {m_filter = Just [0]}
  , (def_meta "TwoZero") {m_filter = Just [0]}
  , (def_meta "UnaryOpUGen") {m_filter = Just [0]}
  , (def_meta "VarLag") {m_filter = Just [0], m_n_outputs = Just 1}
  , (def_meta "VBAP") {m_nc_input = True}
  , (def_meta "VDiskIn") {m_enumerations = Just [(2, "Loop")], m_nc_input = True, m_fixed_rate = Just ar}
  , (def_meta "Vibrato") {m_nondet = True}
  , (def_meta "Warp1") {m_fixed_rate = Just ar, m_nc_input = True}
  , (def_meta "WarpZ") {m_nc_input = True}
  , (def_meta "WhiteNoise") {m_nondet = True}
  , (def_meta "Wrap") {m_filter = Just [0]}
  , (def_meta "WrapIndex") {m_filter = Just [1]}
  , (def_meta "XFade2") {m_filter = Just [0, 1]}
  , (def_meta "XLine") {m_enumerations = Just [(3, "DoneAction")]}
  , (def_meta "XOut") {m_filter = Just [2], m_std_mce = 1, m_n_outputs = Just 0}
  , (def_meta "ZeroCrossing") {m_filter = Just [0]}
  ]

-- | Encoding of 'Meta' for external UGens.
meta_data_ext :: [Meta]
meta_data_ext =
  [ (def_meta "AnalogPhaser") {m_filter = Just [0]}
  , (def_meta "AnalogPhaserMod") {m_filter = Just [0]}
  , (def_meta "AnalogTape") {m_filter = Just [0]}
  , (def_meta "AnalogVintageDistortion") {m_filter = Just [0]}
  , (def_meta "ArrayMax") {m_filter = Just [0], m_std_mce = 1}
  , (def_meta "ArrayMin") {m_filter = Just [0], m_std_mce = 1}
  , (def_meta "AudioMSG") {m_filter = Just [0]}
  , (def_meta "AverageOutput") {m_filter = Just [0]}
  , (def_meta "BeatTrack") {m_n_outputs = Just 4}
  , (def_meta "BlitB3") {m_unipolar = True}
  , (def_meta "BMoog") {m_filter = Just [0]}
  , (def_meta "CrossoverDistortion") {m_filter = Just [0]}
  , (def_meta "DCompressor") {m_filter = Just [0]}
  , (def_meta "DFM1") {m_filter = Just [0]}
  , (def_meta "Disintegrator") {m_nondet = True, m_filter = Just [0]}
  , (def_meta "Dneuromodule") {m_fixed_rate = Just dr, m_nondet = True, m_nc_input = True, m_std_mce = 3}
  , (def_meta "DoubleNestedAllpassC") {m_filter = Just [0]}
  , (def_meta "DoubleNestedAllpassL") {m_filter = Just [0]}
  , (def_meta "DoubleNestedAllpassN") {m_filter = Just [0]}
  , (def_meta "DWGSoundBoard") {m_filter = Just [0]}
  , (def_meta "FM7") {m_std_mce = 2}
  , (def_meta "FMGrain") {m_filter = Just [0]}
  , (def_meta "FMGrainB") {m_filter = Just [0]}
  , (def_meta "Gammatone") {m_filter = Just [0]}
  , (def_meta "GaussTrig") {m_unipolar = True}
  , (def_meta "GlitchRHPF") {m_filter = Just [0]}
  , (def_meta "GreyholeRaw") {m_filter = Just [0, 1]}
  , (def_meta "HairCell") {m_filter = Just [0]}
  , (def_meta "HarmonicOsc") {m_std_mce = 1}
  , (def_meta "IirFilter") {m_filter = Just [0]}
  , (def_meta "LFBrownNoise0") {m_nondet = True}
  , (def_meta "LFBrownNoise1") {m_nondet = True}
  , (def_meta "LFBrownNoise2") {m_nondet = True}
  , (def_meta "Lores") {m_filter = Just [0]}
  , (def_meta "LPCAnalyzer") {m_filter = Just [0, 1], m_fixed_rate = Just ar}
  , (def_meta "LPCSynth") {m_fixed_rate = Just ar}
  , (def_meta "LPCVals") {m_fixed_rate = Just ar, m_n_outputs = Just 3}
  , (def_meta "LPG") {m_filter = Just [0]}
  , (def_meta "Meddis") {m_filter = Just [0]}
  , (def_meta "MiClouds") {m_input_reorder = reorder_first_last 15, m_std_mce = 1}
  , (def_meta "MiRipples") {m_filter = Just [0]}
  , (def_meta "MiVerb") {m_input_reorder = reorder_first_last 7, m_std_mce = 1, m_filter = Just [6]}
  , (def_meta "MoogLadder") {m_filter = Just [0]}
  , (def_meta "MoogVCF") {m_filter = Just [0]}
  , (def_meta "NestedAllpassC") {m_filter = Just [0]}
  , (def_meta "NestedAllpassL") {m_filter = Just [0]}
  , (def_meta "NestedAllpassN") {m_filter = Just [0]}
  , (def_meta "OteySoundBoard") {m_filter = Just [0]}
  , (def_meta "PhasorModal") {m_filter = Just [0]}
  , (def_meta "Bezier") {m_std_mce = 1}
  , (def_meta "RCD") {m_filter = Just [0]}
  , (def_meta "Resonator") {m_filter = Just [0]}
  , (def_meta "RDelayMap") {m_filter = Just [1], m_std_mce = 1}
  , (def_meta "RDelaySet") {m_filter = Just [0], m_std_mce = 1}
  , (def_meta "RDelaySetBuf") {m_std_mce = 1}
  , (def_meta "RDL") {m_std_mce = 1}
  , (def_meta "DustRange") {m_nondet = True, m_fixed_rate = Just ar}
  , (def_meta "ExpRandN") {m_nondet = True, m_fixed_rate = Just ir, m_nc_input = True}
  , (def_meta "Freezer") {m_fixed_rate = Just ar}
  , (def_meta "IRandN") {m_nondet = True, m_fixed_rate = Just ir, m_nc_input = True}
  , (def_meta "RLagC") {m_filter = Just [0]}
  , (def_meta "LinRandN") {m_nondet = True, m_fixed_rate = Just ir, m_nc_input = True}
  , (def_meta "RMEQ") {m_filter = Just [0]}
  , (def_meta "ObxdFilter") {m_filter = Just [0]}
  , (def_meta "RPVDecayTbl") {m_fixed_rate = Just kr}
  , (def_meta "RandN") {m_nondet = True, m_fixed_rate = Just ir, m_nc_input = True}
  , (def_meta "ShufflerB") {m_fixed_rate = Just ar}
  , (def_meta "RShufflerL") {m_filter = Just [0]}
  , (def_meta "SvfbP") {m_filter = Just [0]}
  , (def_meta "SvfHp") {m_filter = Just [0]}
  , (def_meta "SvflP") {m_filter = Just [0]}
  , (def_meta "TExpRandN") {m_nondet = True, m_filter = Just [2], m_nc_input = True}
  , (def_meta "TLinRandN") {m_nondet = True, m_filter = Just [3], m_nc_input = True}
  , (def_meta "TRandN") {m_nondet = True, m_filter = Just [2], m_nc_input = True}
  , (def_meta "SMS") {m_filter = Just [0], m_fixed_rate = Just ar}
  , (def_meta "SineShaper") {m_filter = Just [0]}
  , (def_meta "SoftClipAmp") {m_filter = Just [0]}
  , (def_meta "SoftClipAmp4") {m_filter = Just [0]}
  , (def_meta "SoftClipAmp8") {m_filter = Just [0]}
  , (def_meta "SpectralEntropy") {m_nc_input = True}
  , (def_meta "Squiz") {m_filter = Just [0]}
  , (def_meta "StkInst") {m_std_mce = 1, m_input_reorder = Just [4, 0, 1, 2, 3, 5]}
  , (def_meta "Streson") {m_filter = Just [0]}
  , (def_meta "Summer") {m_filter = Just [0]}
  , (def_meta "SVF") {m_filter = Just [0]}
  , (def_meta "SwitchDelay") {m_filter = Just [0]}
  , (def_meta "TBetaRand") {m_filter = Just [4], m_nondet = True}
  , (def_meta "TBrownRand") {m_filter = Just [4], m_nondet = True}
  , (def_meta "TGaussRand") {m_filter = Just [2], m_nondet = True}
  , (def_meta "TPV") {m_fixed_rate = Just ar}
  , (def_meta "TScramble") {m_nondet = True, m_filter = Just [0], m_std_mce = 1, m_nc_mce = Just 0}
  , (def_meta "VBFourses") {m_n_outputs = Just 4, m_input_reorder = reorder_swap, m_std_mce = 1}
  , (def_meta "VBJonVerb") {m_filter = Just [0]}
  , (def_meta "VBSlide") {m_filter = Just [0]}
  , (def_meta "VOSIM") {m_unipolar = True}
  ]

meta_data :: [Meta]
meta_data = meta_data_core ++ meta_data_ext

-- > map meta_data_lookup ["SinOsc","VarLag"]
meta_data_lookup :: String -> Maybe Meta
meta_data_lookup nm = find ((== nm) . m_name) meta_data

-- > map meta_data_lookup_def ["SinOsc","VarLag"]
meta_data_lookup_def :: String -> Meta
meta_data_lookup_def nm = fromMaybe (def_meta nm) (meta_data_lookup nm)

-- * Predicates

m_has_var_inputs :: Meta -> Bool
m_has_var_inputs m = m_nc_input m || isJust (m_nc_mce m)

m_has_mce_rule :: Meta -> Bool
m_has_mce_rule m = isJust (m_nc_mce m) || (m_std_mce m > 0) || isJust (m_secondary_mce m)
