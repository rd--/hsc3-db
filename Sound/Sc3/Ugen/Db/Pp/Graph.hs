-- | Pretty printers for Ugen graphs.
module Sound.Sc3.Ugen.Db.Pp.Graph where

import qualified Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import System.FilePath {- filepath -}
import System.Process {- process -}

import Sound.Sc3.Ugen.Types {- hsc3 -}

import qualified Sound.Sc3.Common.Math as Math {- hsc3 -}
import qualified Sound.Sc3.Common.Mce as Mce {- hsc3 -}
import qualified Sound.Sc3.Common.Rate as Rate {- hsc3 -}
import qualified Sound.Sc3.Server.Graphdef.Binary as Graphdef {- hsc3 -}
import qualified Sound.Sc3.Server.Graphdef.Read as Read {- hsc3 -}
import qualified Sound.Sc3.Ugen.Graph.Reconstruct as Reconstruct {- hsc3 -}
import qualified Sound.Sc3.Ugen.Ugen as Ugen {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Core as Bindings {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Meta as Meta {- hsc3-db -}

-- * Util

-- | True if Ugen name is UnaryOpUgen or BinaryOpUgen.
is_operator :: String -> Bool
is_operator = flip elem ["UnaryOpUgen", "BinaryOpUgen"]

interleave :: [a] -> [a] -> [a]
interleave p q = let u (i, j) = [i, j] in concatMap u (zip p q)

-- * Param

param_nm_join :: Bindings.Param_Nm -> [String]
param_nm_join (i, j) = i ++ fromMaybe [] j

{- | Ugen param name

>>> map ugen_param_nm (words "Out Pan2 BinaryOpUGen")
[(["bus"],Just ["channelsArray"]),(["in","pos","level"],Nothing),(["a","b"],Nothing)]
-}
ugen_param_nm :: String -> Bindings.Param_Nm
ugen_param_nm nm =
  if nm == "BinaryOpUGen"
  then (["a", "b"], Nothing)
  else if nm == "UnaryOpUGen"
       then (["a"], Nothing)
       else Bindings.u_param_nm (Db.u_lookup_cs_err nm)

-- * Forth

{- | Print Forth (reverse polish) notation for graph, see hsc3-forth
   for details.  The flag controls printing of 'Uid' entries.
   This should, but does not, print the Mce instruction for primitives
   that halt mce expansion, ie. ENVGEN and so on.

>>> import Sound.Sc3
>>> ugen_graph_forth_pp (False,True) (sinOsc ar 440 0)
"( freq ) 440 ( phase ) 0 SinOsc.ar"

>>> ugen_graph_forth_pp (False,True) (tRandId 'α' 0 1 (impulse kr 1 0))
"( lo ) 0 ( hi ) 1 ( trig ) ( freq ) 1 ( phase ) 0 Impulse.kr TRand"
-}
ugen_graph_forth_pp :: (Bool, Bool) -> Ugen -> String
ugen_graph_forth_pp (print_uid, print_kw) u =
  let recur = ugen_graph_forth_pp (print_uid, print_kw)
      prim_pp (Primitive rt nm i _ sp k _) =
        let m = Meta.meta_data_lookup_def nm
            p = param_nm_join (ugen_param_nm nm)
            in_paren x = "( " ++ x ++ " )"
            rt' =
              if is_operator nm || isJust (Meta.m_filter m)
                then ""
                else '.' : map Data.Char.toLower (Rate.rateAbbrev rt)
            nm' = concat [Ugen.ugen_user_name nm sp, rt']
            k' = case k of
              NoId -> Nothing
              Uid uid -> if print_uid then Just (show uid ++ " uid") else Nothing
            i' = (if print_kw then interleave (map in_paren p) else id) (map recur i)
        in unwords (i' ++ catMaybes [k', Just nm'])
  in case u of
      Constant_U (Constant n _) -> Math.real_pp 5 n
      Control_U (Control _ _ nm _ _ _ _) -> "ctl:" ++ nm
      Label_U (Label s) -> concat ["s\" ", show s, "\""]
      Primitive_U p -> prim_pp p
      Proxy_U (Proxy p n) -> prim_pp p ++ "@" ++ show n
      Mce_U (Mce.Mce_Scalar u') -> recur u'
      Mce_U m ->
        let v = Mce.mce_to_list m
        in if mce_is_direct_proxy m
            then prim_pp (proxySource (fromJust (un_proxy (head v))))
            else unwords (map recur v ++ [show (length v), "array"])
      Mrg_U (Mrg l r) -> unwords [recur l, recur r, "2", "mrg"]

-- * Smalltalk

{- | Simple Ugen graph pretty printer as Smalltalk code.

>>> import Sound.Sc3
>>> pp = ugen_graph_smalltalk_pp
>>> pp (sinOsc ar 440 0 + lfSaw ar 220 0)
"(+ a: (SinOsc freq: 440 phase: 0) b: (LFSaw freq: 220 iphase: 0))"

>>> pp (sinOsc ar (mce2 440 441) 0)
"{(SinOsc freq: 440 phase: 0) . (SinOsc freq: 441 phase: 0)} mce"

>>> pp (out (control kr "out" 0) (pan2 (sinOsc ar 440 0) 0 1))
"(Out bus: (Control name: 'out' init: 0.0) channelsArray: (Pan2 in: (SinOsc freq: 440 phase: 0) pos: 0 level: 1) . (Pan2 in: (SinOsc freq: 440 phase: 0) pos: 0 level: 1))"

>>> pp (tRandId 'α' 0 1 (impulse kr 1 0))
"(TRand lo: 0 hi: 1 trig: (Impulse freq: 1 phase: 0))"

>>> pp (envGen kr 1 1 0 1 DoNothing (envTrapezoid 0.5 0.75 0.65 0.35))
"(EnvGen gate: 1 levelScale: 1 levelBias: 0 timeScale: 1 doneAction: 0 envelope: 0 . 3 . -99 . -99 . 0.35 . 0.24375 . 1 . 0 . 0.35 . 0.325 . 1 . 0 . 0 . 0.08125 . 1 . 0)"
-}
ugen_graph_smalltalk_pp :: Ugen -> String
ugen_graph_smalltalk_pp u =
  let in_paren x = '(' : x ++ ")"
      in_braces x = '{' : x ++ "}"
      as_keyword x = x ++ ":"
      recur = ugen_graph_smalltalk_pp
      prim_pp (Primitive _ nm i _ sp _ _) =
        let usr = Ugen.ugen_user_name nm sp
        in if is_operator nm
            then case i of
              [lhs] -> printf "%s %s" (recur lhs) usr
              [lhs, rhs] -> printf "%s %s %s" (recur lhs) usr (recur rhs)
              _ -> error "ugen_graph_smalltalk_pp: operator?"
            else
              let p = param_nm_join (ugen_param_nm nm)
                  i' = interleave (map as_keyword p ++ repeat ".") (map recur i)
              in in_paren (unwords (usr : i'))
  in case u of
      Constant_U (Constant n _) -> Math.real_pp 5 n
      Control_U (Control _ _ nm df _ _ _) -> printf "(Control name: '%s' init: %f)" nm df
      Label_U (Label s) -> printf "'%s'" s
      Primitive_U p -> prim_pp p
      Proxy_U (Proxy p _) -> prim_pp p
      Mce_U (Mce.Mce_Scalar u') -> recur u'
      Mce_U m ->
        let v = Mce.mce_to_list m
        in if mce_is_direct_proxy m
            then prim_pp (proxySource (fromJust (un_proxy (head v))))
            else in_braces (intercalate " . " (map recur v)) ++ " mce"
      Mrg_U (Mrg l r) -> printf "{%s . %s}" (recur l) (recur r)

-- * SuperCollider

{- | Simple Ugen graph pretty printer as SuperCollider code.

>>> import Sound.Sc3
>>> pp = ugen_graph_supercollider_pp
>>> pp (sinOsc ar 440 0 + lfSaw ar 220 0)
"+.ar(a: SinOsc.ar(freq: 440, phase: 0), b: LFSaw.ar(freq: 220, iphase: 0))"

>>> pp (sinOsc ar (mce2 440 441) 0)
"[SinOsc.ar(freq: 440, phase: 0), SinOsc.ar(freq: 441, phase: 0)]"

>>> pp (out (control kr "out" 0) (pan2 (sinOsc ar 440 0) 0 1))
"Out.ar(bus: NamedControl.kr(name: 'out', values: 0.0), channelsArray: Pan2.ar(in: SinOsc.ar(freq: 440, phase: 0), pos: 0, level: 1))"

>>> pp (tRandId 'α' 0 1 (impulse kr 1 0))
"TRand.kr(lo: 0, hi: 1, trig: Impulse.kr(freq: 1, phase: 0))"

>>> pp (envGen kr 1 1 0 1 DoNothing (envTrapezoid 0.5 0.75 0.65 0.35))
"EnvGen.kr(gate: 1, levelScale: 1, levelBias: 0, timeScale: 1, doneAction: 0, envelope: [0, 3, -99, -99, 0.35, 0.24375, 1, 0, 0.35, 0.325, 1, 0, 0, 0.08125, 1, 0])"
-}
ugen_graph_supercollider_pp :: Ugen -> String
ugen_graph_supercollider_pp u =
  let jn i j = concat [i, " ", j]
      in_paren x = '(' : x ++ ")"
      in_brackets x = '[' : x ++ "]"
      as_keyword x = x ++ ":"
      recur = ugen_graph_supercollider_pp
      prim_pp (Primitive rt nm i _ sp _ _) =
        let usr = Ugen.ugen_user_name nm sp
        in if is_operator nm
            then case i of
              [lhs] -> printf "%s %s" (recur lhs) usr -- ?
              [lhs, rhs] ->
                if any Data.Char.isLetter usr
                  then printf "%s.%s(%s)" (recur lhs) (map Data.Char.toLower usr) (recur rhs)
                  else printf "%s %s %s" (recur lhs) usr (recur rhs)
              _ -> error "ugen_graph_supercollider_pp: operator?"
            else
              let p = param_nm_join (ugen_param_nm nm)
                  p_n = length p
                  i_n = length i
                  i' = map recur (if i_n > p_n then take (p_n - 1) i ++ [mce (drop (p_n - 1) i)] else i)
                  z = zipWith jn (map as_keyword p) (take p_n i')
              in printf "%s.%s" usr (Rate.rateAbbrev rt) ++ in_paren (intercalate ", " z)
  in case u of
      Constant_U (Constant n _) -> Math.real_pp 5 n
      Control_U (Control _ _ nm df _ _ _) -> printf "NamedControl.kr(name: '%s', values: %f)" nm df
      Label_U (Label s) -> printf "'%s'" s
      Primitive_U p -> prim_pp p
      Proxy_U (Proxy p _) -> prim_pp p
      Mce_U (Mce.Mce_Scalar u') -> recur u'
      Mce_U m ->
        let v = Mce.mce_to_list m
        in if mce_is_direct_proxy m
            then prim_pp (proxySource (fromJust (un_proxy (head v))))
            else in_brackets (intercalate ", " (map recur v))
      Mrg_U (Mrg l r) -> printf "[%s, %s]" (recur l) (recur r)

-- * Scsyndef

{- | Read a scsyndef file and pretty print it using /pp/.
     Generates a temporary file that has a Plain Ugen definition of the input as a Ugen.
-}
scsyndef_to_pp :: String -> FilePath -> FilePath -> FilePath -> IO ()
scsyndef_to_pp pp tmp_dir sy_fn pp_fn = do
  gr <- Graphdef.read_graphdef_file sy_fn
  let to_name :: String -> String
      to_name = ("sy_" ++) . map (\c -> if c `elem` "-." then '_' else c)
      nm = to_name (dropExtension (takeFileName sy_fn))
      (_, gr') = Read.graphdef_to_graph gr
      hs = Reconstruct.reconstruct_graph_module nm gr'
      imp = "import qualified Sound.Sc3.Ugen.Db.Pp as Pp {- hsc3-db -}"
      mn =
        [ "main :: IO ()"
        , "main = putStrLn (Pp." ++ pp ++ " " ++ nm ++ ")"
        ]
      hs_fn = tmp_dir </> "scsyndef-to-pp.hs"
  writeFile hs_fn (unlines (imp : hs ++ mn))
  _ <- system ("runhaskell " ++ hs_fn ++ " > " ++ pp_fn)
  return ()

{- | Read a scsyndef file and write it in hsc3-forth notation.

> let sy = "/home/rohan/sw/hsc3-graphs/db/aeb8317e8310371b.scsyndef"
> scsyndef_to_fs "/tmp" sy "/dev/stdout"
-}
scsyndef_to_fs :: FilePath -> FilePath -> FilePath -> IO ()
scsyndef_to_fs = scsyndef_to_pp "ugen_graph_forth_pp (False,False)"

{- | Read a scsyndef file and write it in stsc3 notation.

> let sy = "/home/rohan/sw/hsc3-graphs/db/aeb8317e8310371b.scsyndef"
> scsyndef_to_st "/tmp" sy "/dev/stdout"
-}
scsyndef_to_st :: FilePath -> FilePath -> FilePath -> IO ()
scsyndef_to_st = scsyndef_to_pp "ugen_graph_smalltalk_pp"

{- | Read a scsyndef file and write it in SuperCollider notation.

> let sy = "/home/rohan/sw/hsc3-graphs/db/aeb8317e8310371b.scsyndef"
> scsyndef_to_sc "/tmp" sy "/dev/stdout"
-}
scsyndef_to_sc :: FilePath -> FilePath -> FilePath -> IO ()
scsyndef_to_sc = scsyndef_to_pp "ugen_graph_supercollider_pp"
