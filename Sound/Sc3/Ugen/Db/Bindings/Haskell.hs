-- | Generate (approximate) Haskell UGen binding functions from DB.
module Sound.Sc3.Ugen.Db.Bindings.Haskell where

import Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import Sound.Sc3.Common.Rate {- hsc3 -}

import Sound.Sc3.Ugen.Db.Record {- hsc3-db -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Core as Bindings {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Pp as Pp {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Rename {- hsc3-db -}

-- * Utilities

{- | Remove trailing 'isSpace'.

>>> map delete_trailing_whitespace ["x ","y\t","z\n"]
["x","y","z"]
-}
delete_trailing_whitespace :: String -> String
delete_trailing_whitespace = reverse . dropWhile isSpace . reverse

{- | Bracket list.

>>> about ('[',']') "a,b"
"[a,b]"
-}
about :: (a, a) -> [a] -> [a]
about (p, q) s = p : s ++ [q]

{- | 'about' of @[]@.

>>> brckt "a,b"
"[a,b]"
-}
brckt :: String -> String
brckt = about ('[', ']')

-- | 'about' of @""@.
quote :: [Char] -> [Char]
quote = about ('"', '"')

{- | Variant that 'delete's empty inputs, useful for pretty printing.

>>> unwords ["a","","b"]
"a  b"

>>> unwords_rm ["a","","b"]
"a b"
-}
unwords_rm :: [String] -> String
unwords_rm = unwords . filter (not . null)

-- * Bindings

{- | Given name of 'Enum' give name of function to map to UGen input.

>>> unenumerator "Warp"
"from_warp"

>>> import Sound.Sc3.Common.Enum
>>> from_warp Linear
0
-}
unenumerator :: String -> String
unenumerator en =
  case en of
    "Envelope" -> "envelope_to_ugen"
    "IEnvelope" -> "envelope_to_ienvgen_ugen"
    "Loop" -> "from_loop"
    "Interpolation" -> "from_interpolation"
    "DoneAction" -> "from_done_action"
    "Warp" -> "from_warp"
    _ -> error "unenumerator"

-- | If input is an enumeration add 'unenumerator' as prefix, else perhaps default unwrap.
input_name_proc :: Maybe String -> U -> (String, Int) -> String
input_name_proc unwrap u (nm, ix) =
  case input_enumeration u ix of
    Just en -> printf "(%s %s)" (unenumerator en) nm
    Nothing -> maybe nm (\w -> printf "(%s %s)" w nm) unwrap

-- | 'input_name_proc' of 'ugen_inputs'
u_input_names_proc_plain :: Maybe String -> U -> [String] -> [String]
u_input_names_proc_plain unwrap u nm = zipWith (curry (input_name_proc unwrap u)) nm [0 ..]

{- | 'input_name_proc' of 'Rename.u_renamed_inputs'

>>> u_input_names_proc_rename Nothing (Db.u_lookup_cs_err "Line")
["start","end","dur","(from_done_action doneAction)"]
-}
u_input_names_proc_rename :: Maybe String -> U -> [String]
u_input_names_proc_rename unwrap u = u_input_names_proc_plain unwrap u (Rename.u_renamed_inputs u)

{- | Haskel list Pp.

>>> ppl_list ["freq","phase"]
"[freq,phase]"
-}
ppl_list :: [String] -> String
ppl_list = brckt . intercalate ","

{- | U input types

>>> u_input_types "UGen" (Db.u_lookup_ci_err "MouseX")
["UGen","UGen","Warp UGen","UGen"]
-}
u_input_types :: String -> U -> [String]
u_input_types u_type u =
  let f = maybe u_type (++ " UGen") . input_enumeration u
  in map f [0 .. length (ugen_inputs u) - 1]

{- | Generate HS name for U.

> mapM_ (\u -> putStrLn (unwords [ugen_hs_name u,ugen_name u])) Db.ugen_db
-}
ugen_hs_name :: U -> String
ugen_hs_name = Rename.fromSc3Name . ugen_name

data NonDetMode = NonDetIdentifier | NonDetMonad | NonDetUnsafe deriving (Eq)

nondetName :: (U, NonDetMode) -> String
nondetName (u, nd) =
  let nm = ugen_hs_name u
  in if ugen_nondet u
      then case nd of
        NonDetIdentifier -> nm ++ "Id"
        NonDetMonad -> nm ++ "M"
        NonDetUnsafe -> nm ++ "U"
      else nm

u_has_rate_param :: U -> Bool
u_has_rate_param u =
  case ugen_filter u of
    Nothing -> isNothing (ugen_fixed_rate u)
    Just _ -> False

{- | Generate haskell type signature for UGen constructor.

>>> pp = unwords . u_gen_type_sig NonDetUnsafe . Db.u_lookup_cs_err
>>> pp "Blip" -- oscillator
"blip :: Rate -> UGen -> UGen -> UGen"

>>> pp "A2K" -- fixed rate
"a2k :: UGen -> UGen"

>>> pp "BufRd" -- variable channel oscillator
"bufRd :: Int -> UGen -> UGen -> Loop UGen -> Interpolation UGen -> UGen"

>>> pp "Resonz" -- filter
"resonz :: UGen -> UGen -> UGen -> UGen"

>>> pp "BrownNoise" -- non-deterministic
"brownNoiseU :: Rate -> UGen"

>>> pp "Dseq" -- fixed rate / non-det
"dseqU :: UGen -> UGen -> UGen"

>>> pp "EnvGen" -- enum
"envGen :: Rate -> UGen -> UGen -> UGen -> UGen -> DoneAction UGen -> Envelope UGen -> UGen"
-}
u_gen_type_sig :: NonDetMode -> U -> [String]
u_gen_type_sig nd u =
  let i_sig = u_input_types "UGen" u
      nm = [nondetName (u, nd), "::"]
      (nd_constr, nd_var, res_qual) =
        if ugen_nondet u
          then case nd of
            NonDetIdentifier -> (["ID", "a", "=>"], ["a", "->"], [])
            NonDetMonad -> (["UId m =>"], [], ["m"])
            NonDetUnsafe -> ([], [], [])
          else ([], [], [])
      o = if ugen_nc_input u then ["Int", "->"] else []
      r = if u_has_rate_param u then ["Rate", "->"] else []
      i_sig' = intersperse "->" i_sig
      arr = if null i_sig then [] else ["->"]
  in concat [nm, nd_constr, o, nd_var, r, i_sig', arr, res_qual, ["UGen"]]

{- | The @outputs@ field may be fixed (ie. @SinOsc@) or variable
     (ie. @In@ or @Demand@).  The output is a @(lhs,rhs)@ pair, either
    @("nc","nc")@ or @("","k")@.

>>> u_outputs (Db.u_lookup_cs_err "BufRd")
("numChannels","numChannels")

>>> u_outputs (Db.u_lookup_cs_err "SinOsc")
("","1")

>>> u_outputs (Db.u_lookup_cs_err "Demand")
("","(length (mceChannels demandUGens) + 0)")
-}
u_outputs :: U -> (String, String)
u_outputs u =
  case u_fixed_outputs u of
    Just d -> ("", show d)
    Nothing ->
      if ugen_nc_input u
        then ("numChannels", "numChannels")
        else case ugen_nc_mce u of
          Just j ->
            let i = input_name (last (ugen_inputs u))
                j' = show j
            in ("", concat ["(length (mceChannels ", i, ") + ", j', ")"])
          Nothing -> error "u_outputs?" -- rhs...

{- | Generate oscillator UGen constructor.

> let f = u_gen_fun NonDetIdentifier . Rename.u_rename . u_lookup_cs_err
> let g = mapM_ (putStrLn . unwords_rm) . map f
> g (words "Blip BufRd Dseq Rand WhiteNoise")
> g (words "CoinGate HPZ1 Out Resonz")
-}
u_gen_fun :: NonDetMode -> U -> [String]
u_gen_fun nd u =
  let nm_sc3 = ugen_name u
      nm_hs = nondetName (u, nd)
      i_lhs = Rename.u_renamed_inputs u
      (i_rhs, mc) = case Bindings.bindings_mce u (u_input_names_proc_rename Nothing u) of
        (r, Nothing) -> (ppl_list r, "Nothing")
        (r, Just mc') -> (ppl_list r, concat ["(Just [", intercalate "," mc', "])"])
      (nd_lhs, nd_rhs) =
        if ugen_nondet u
          then case nd of
            NonDetIdentifier -> ("z", "(toUId z)")
            _ -> error "u_gen_fun?"
          else ("", "NoId")
      r_set =
        let rr = case ugen_operating_rates u of
              [] -> all_rates
              r' -> r'
        in map show rr
      (o_lhs, o_rhs) = u_outputs u
      (rt_lhs, rt_rhs) = case ugen_filter u of
        Just ix' -> ("", concat ["(Right ", ppl_list (map show ix'), ")"])
        Nothing -> case ugen_fixed_rate u of
          Just rt' -> ("", "(Left " ++ show rt' ++ ")")
          Nothing -> ("rate", "(Left rate)")
  in concat
      [ [nm_hs, o_lhs, nd_lhs, rt_lhs]
      , i_lhs
      , ["=", "mkUGen", "Nothing"]
      , [ppl_list r_set, rt_rhs, quote nm_sc3, i_rhs, mc, o_rhs, "(Special 0)", nd_rhs]
      ]

{- | Binding as @[haddock-comment,haddock-summary,type-signature,function-definition]@.

> u_gen_binding (Db.u_lookup_cs_err "LFGauss")
> u_gen_binding (Db.u_lookup_cs_err "In")
> u_gen_binding (Db.u_lookup_cs_err "EnvGen")
> u_gen_binding (Db.u_lookup_cs_err "LinExp")
> u_gen_binding (Db.u_lookup_cs_err "FM7")
-}
u_gen_binding :: U -> [String]
u_gen_binding u' =
  let u = Rename.u_rename_db Db.ugen_db u'
      c = ["-- |", delete_trailing_whitespace (ugen_summary u)]
      h = ["--\n-- ", delete_trailing_whitespace (intercalate ";" (lines (Pp.u_summary u')))]
      s = u_gen_type_sig NonDetIdentifier u
      b = u_gen_fun NonDetIdentifier u
  in map unwords_rm [c, h, s, b]

u_n_param :: U -> Int
u_n_param u = sum [length (ugen_inputs u), if u_has_rate_param u then 1 else 0, if ugen_nc_input u then 1 else 0]

u_gen_monad_cons :: U -> [String]
u_gen_monad_cons u =
  [ printf "-- | Monad variant of %s." (ugen_name u)
  , unwords (u_gen_type_sig NonDetMonad u)
  , printf "%s = liftUId%d %s" (nondetName (u, NonDetMonad)) (u_n_param u) (nondetName (u, NonDetIdentifier))
  ]

u_gen_unsafe_cons :: U -> [String]
u_gen_unsafe_cons u =
  [ printf "-- | Unsafe variant of %s." (ugen_name u)
  , unwords (u_gen_type_sig NonDetUnsafe u)
  , printf "%s = liftUnsafe%d %s" (nondetName (u, NonDetUnsafe)) (u_n_param u) (nondetName (u, NonDetMonad))
  ]

{-
> u_gen_binding_set (Db.u_lookup_cs_err "BrownNoise") -- non-deterministic
-}
u_gen_binding_set :: U -> [String]
u_gen_binding_set u =
  if ugen_nondet u
    then concat [u_gen_binding u, [""], u_gen_monad_cons u, [""], u_gen_unsafe_cons u]
    else u_gen_binding u

-- | 'u_gen_binding_set' of 'ugen_db'.
u_gen_bindings :: [U] -> [String]
u_gen_bindings = intercalate [""] . map u_gen_binding_set

u_bindings_preamble :: [String]
u_bindings_preamble =
  [ "-- | Sc3.Ugen bindings (auto-generated)."
  , "module Sound.Sc3.Ugen.Bindings.Db where"
  , ""
  , "import Sound.Sc3.Common.Enum"
  , "import Sound.Sc3.Common.Envelope"
  , "import Sound.Sc3.Common.Rate"
  , "import Sound.Sc3.Common.UId"
  , "import Sound.Sc3.Common.Unsafe"
  , "import Sound.Sc3.Ugen.Type"
  , "import Sound.Sc3.Ugen.UGen"
  , ""
  ]

{- | 'writeFile' of 'u_gen_bindings'.

> let fn = "/home/rohan/tmp/hsc3-bindings-core.hs"
> let fn = "/home/rohan/sw/hsc3/Sound/Sc3.Ugen/Bindings/Db.hs"
> u_gen_bindings_write fn Db.ugen_db_core

> let fn = "/home/rohan/tmp/hsc3-bindings-ext.hs"
> let fn = "/home/rohan/sw/hsc3/Sound/Sc3.Ugen/Bindings/Db/External.hs"
> u_gen_bindings_write fn Db.ugen_db_ext
-}
u_gen_bindings_write :: FilePath -> [U] -> IO ()
u_gen_bindings_write fn = writeFile fn . unlines . (++) u_bindings_preamble . u_gen_bindings
