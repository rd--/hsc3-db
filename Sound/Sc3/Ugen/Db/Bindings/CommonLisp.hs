-- | CommonLisp (CLOS) UGen Bindings
module Sound.Sc3.Ugen.Db.Bindings.CommonLisp where

import Data.Char {- base -}
-- import Data.List {- base -}

import Data.Maybe {- base -}
import Text.Printf {- base -}

import Sound.Sc3.Common.Rate {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as DB {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Core as Bindings {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as DB {- hsc3-db -}

-- > putStrLn $ cl_defclass "SinOsc" "Sine oscillator" (["freq","phase"],Nothing) ([440,0],Nothing)
-- > putStrLn $ cl_defclass "UnaryOp" "Unary operator" (["a"],Nothing) ([0],Nothing)
-- > putStrLn $ cl_defclass "BinaryOp" "Binary operator" (["a","b"],Nothing) ([0,0],Nothing)
cl_defclass :: String -> String -> Bindings.Param_Nm -> Bindings.Param_Def -> String
cl_defclass ugen_nm ugen_dsc param_nm param_def =
  (unlines . concat)
    [
      [ printf "(defclass %s (ugen)" (map toLower ugen_nm)
      , "  ("
      ]
    , zipWith
        (\k v -> printf "   (%s :initarg :%s :initform %f)" k k v)
        (Bindings.all_param_ty param_nm)
        (Bindings.all_param_ty param_def)
    ,
      [ "  )"
      , printf "  (:documentation \"%s\"))" ugen_dsc
      ]
    ]

-- > unwords (map st_rate_id [minBound .. maxBound]) == "#ir #kr #ar #dr"
cl_rate_id :: Rate -> String
cl_rate_id = ('\'' :) . map toLower . show

-- > putStrLn $ cl_defun "SinOsc" AR (Right 1) 0 (["freq","phase"],Nothing) ([440,0],Nothing)
cl_defun :: String -> Rate -> Bindings.Num_Chan -> Int -> Bindings.Param_Nm -> Bindings.Param_Def -> String
cl_defun ugen_nm ugen_rt num_chan special_index param_nm param_def =
  (unlines . concat)
    [
      [ printf "(defun %s" (map toLower ugen_nm)
      , " (&key"
      ]
    , zipWith
        (\k v -> printf "  (%s %f)" k v)
        (Bindings.all_param_ty param_nm)
        (Bindings.all_param_ty param_def)
    , if ugen_nm == "MulAdd" then [] else ["  (mul 1.0) (add 0.0)"]
    ,
      [ " )"
      , if ugen_nm == "MulAdd" then "" else " (muladd :in"
      , printf "  (make-instance '%s" (map toLower ugen_nm)
      , printf "   :name \"%s\"" ugen_nm
      , printf "   :param-names '(%s)" (unwords (fst param_nm))
      , printf "   :mce-names '(%s)" (unwords (fromMaybe [] (snd param_nm)))
      , printf "   :rate '%s" (map toLower (show ugen_rt))
      , printf "   :num-chan %s" (Bindings.num_chan_fixed_str "nil" num_chan)
      , printf "   :special-index %d" special_index
      ]
    , map (\k -> printf "  :%s %s" k k) (Bindings.all_param_ty param_nm)
    ,
      [ "  )"
      , if ugen_nm == "MulAdd" then "" else " :mul mul :add add)"
      , ")"
      ]
    ]

{- | Generate CommonLisp definitions for U

> pp = putStrLn . unlines . cl_gen_u . DB.u_lookup_cs_err
> mapM_ pp ["LFGauss","EnvGen","LinExp","FM7","In"]
-}
cl_gen_u :: DB.U -> [String]
cl_gen_u u =
  let ugen_nm = DB.ugen_name u
      ugen_rt = DB.ugen_default_rate u
      -- (length (mceChannels demandUGens) + 0)
      param_nm = Bindings.u_param_nm u
      param_def = Bindings.u_param_def u
      num_chan = Bindings.u_num_chan u
      ugen_dsc = DB.ugen_summary u
  in [ cl_defclass ugen_nm ugen_dsc param_nm param_def
     , cl_defun ugen_nm ugen_rt num_chan 0 param_nm param_def
     , printf "(export '%s)\n" (map toLower ugen_nm)
     ]

{- | Binary operator (defmethod).  Note the rhs (b) is not type-restricted.

> cl_gen_binop ("+",0)
-}
cl_gen_binop :: (String, Int) -> String
cl_gen_binop (sym, ix) =
  printf
    "(defmethod %s ((a ugen) b) (binaryop :a a :b b :special-index %d)) (export '%s)"
    sym
    ix
    sym

{- | Unary operator (defmethod)

> cl_gen_uop ("midicps",17)
-}
cl_gen_uop :: (String, Int) -> String
cl_gen_uop (sym, ix) =
  printf
    "(defmethod %s ((a ugen)) (unaryop :a a :special-index %d)) (export '%s)"
    sym
    ix
    sym

cl_sc3_gen_bindings :: [(String, Int)] -> [(String, Int)] -> [String] -> [String]
cl_sc3_gen_bindings uop binop ugen =
  let f = cl_gen_u . DB.u_lookup_cs_err
  in concat
      [ ["(in-package #:clsc3)", ""]
      , concatMap f ugen
      , map cl_gen_binop binop
      , map cl_gen_uop uop
      ]

cl_sc3_gen_bindings_wr :: FilePath -> [(String, Int)] -> [(String, Int)] -> [String] -> IO ()
cl_sc3_gen_bindings_wr fn uop binop = writeFile fn . unlines . cl_sc3_gen_bindings uop binop
