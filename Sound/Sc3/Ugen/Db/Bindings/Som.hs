-- | Som Ugen Bindings
module Sound.Sc3.Ugen.Db.Bindings.Som where

import System.FilePath {- filepath -}
import Text.Printf {- base -}

import Sound.Sc3.Common.Rate {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Core as Bindings {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Smalltalk as St {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Rename {- hsc3-db -}

{- | Generate beginning of Som class definition.

> putStrLn $ som_class_begin "SinOsc" "Sine oscillator."
> putStrLn $ som_class_begin "LFSaw" "Sawtooth oscillator."
> putStrLn $ som_class_begin "Out" "Write a signal to a bus."
-}
som_class_begin :: String -> String -> String
som_class_begin ugen_nm ugen_dsc =
  unlines
    [ printf "%s = ScUgen (" (Rename.sc3_ugen_rename ugen_nm)
    , "----"
    , "comment = ("
    , printf "\t^'%s'" ugen_dsc
    , ")"
    ]

{- | Generate Som instance creation methods (keyword inputs, with mul: and add:).

> let rw nm rt nc sp pm = putStrLn (som_cons (nm, rt, nc, sp, pm))
> rw "SinOsc" (Left ar) (Right 1) 0 (["freq","phase"],Nothing)
> rw "Pan2" (Right [0]) (Right 2) 0 (["in","pos","level"],Nothing)
> rw "In" (Left ar) (Left (Left ())) 0 (["bus"],Nothing)
> rw "BrownNoise" (Left ar) (Right 1) 0 ([],Nothing)
> rw "MulAdd" (Right [0, 1]) (Right 1) 0 (["in","mul","add"],Nothing)
-}
som_cons :: St.St_Icm -> String
som_cons =
  let fmt (pat, def) = printf "%s = (\n\t%s\n)" pat def
  in unlines . map fmt . St.st_instance_creation_methods

som_gen :: String -> Either Rate [Int] -> String -> Bindings.Num_Chan -> Int -> Bindings.Param_Nm -> String
som_gen ugen_nm ugen_rt ugen_dsc num_chan special param_nm =
  unlines
    [ som_class_begin ugen_nm ugen_dsc
    , som_cons (ugen_nm, ugen_rt, num_chan, special, param_nm)
    , ")"
    ]

{- | Generate Som definitions for U

> pp = putStrLn . som_gen_u . Db.u_lookup_cs_err
> mapM_ pp ["LFGauss","EnvGen","LinExp","FM7","In"]
-}
som_gen_u :: Db.U -> String
som_gen_u u =
  let ugen_nm = Db.ugen_name u
      ugen_rt = maybe (Left (Db.ugen_default_rate u)) Right (Db.ugen_filter u)
      param_nm = Bindings.u_param_nm u
      num_chan = Bindings.u_num_chan u
      ugen_dsc = Db.ugen_summary u
  in som_gen ugen_nm ugen_rt ugen_dsc num_chan 0 param_nm

som_sc3_gen_binding :: String -> String
som_sc3_gen_binding = som_gen_u . Db.u_lookup_cs_err

-- | Som is class-per-file.
som_sc3_gen_binding_wr :: FilePath -> String -> IO ()
som_sc3_gen_binding_wr fn = writeFile fn . som_sc3_gen_binding

som_sc3_gen_bindings_wr :: FilePath -> [String] -> IO ()
som_sc3_gen_bindings_wr dir =
  let wr nm = som_sc3_gen_binding_wr (dir </> Rename.sc3_ugen_rename nm <.> "som") nm
  in mapM_ wr

som_gen_binop :: (String, Int) -> String
som_gen_binop (sym, ix) = printf " %s b = (\n\t^ BinaryOpUGen specialIndex: %d a: self b: b\n)" sym ix

som_gen_uop :: (String, Int) -> String
som_gen_uop (sym, ix) = printf " %s = (\n\t^ UnaryOpUGen specialIndex: %d a: self\n)" sym ix
