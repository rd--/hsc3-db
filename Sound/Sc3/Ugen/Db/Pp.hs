-- | Pretty printers for Ugens and Ugen graphs.
module Sound.Sc3.Ugen.Db.Pp where

import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import Sound.Sc3.Common.Base {- hsc3 -}
import Sound.Sc3.Common.Rate {- hsc3 -}

import qualified Sound.Sc3.Common.Math as Math {- hsc3 -}

import Sound.Sc3.Ugen.Db.Record {- hsc3-db -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Rename {- hsc3-db -}

-- | Pretty printer for 'I', an input at 'U'.
i_pp :: U -> I -> String
i_pp u i =
  let m = if i_is_mce u i then "*" else ""
  in m ++ input_name i ++ "=" ++ Math.real_pp 5 (input_default i)

{- | Pretty printer for 'I'.

>>> let Just u = Db.u_lookup_cs "SinOsc" in i_pp_k u 0
"freq=440"

>>> let Just u = Db.u_lookup_cs "Out" in i_pp_k u 1
"*channelsArray=0"
-}
i_pp_k :: U -> Int -> String
i_pp_k u k = i_pp u (ugen_inputs u !! k)

{- | Generate simple summary string for 'U'.

>>> fmap u_summary (Db.u_lookup_cs "Resonz")
Just "Resonz kr|ar in=0 freq=440 bwr=1\n    Filter: true"

>>> fmap ugen_summary (Db.u_lookup_cs "Resonz")
Just "Resonant filter."
-}
u_summary :: U -> String
u_summary u =
  let commas = intercalate ", "
      mce n = if n > 0 then Just ("Mce=" ++ show n) else Nothing
      nc n = if n then Just ("Nc Input: " ++ show n) else Nothing
      flt _ = "Filter: true"
      sq l = "Reorders Inputs: " ++ show l
      en l = "Enumeration Inputs: " ++ commas (map (\(ix, nm) -> show ix ++ "=" ++ nm) l)
      ps l = "Psuedo Inputs: " ++ show l
      gen f p = fmap f (p u)
      nd (d, b) = if d then Just "Demand/Nondet" else if b then Just "Nondet" else Nothing
      secondary =
        commas
          ( catMaybes
              [ mce (ugen_std_mce u)
              , nc (ugen_nc_input u)
              , gen flt ugen_filter
              , gen sq ugen_reorder
              , gen en ugen_enumerations
              , gen ps ugen_pseudo_inputs
              , nd (u_is_demand_rate u, ugen_nondet u)
              ]
          )
      secondary' = if null secondary then [] else "\n    " ++ secondary
  in unwords
      [ ugen_name u
      , intercalate "|" (map rateAbbrev (ugen_operating_rates u))
      , unwords (map (i_pp u) (ugen_inputs u))
      ]
      ++ secondary'

-- | Lookup named 'Ugen' and generate simple summary string.
ugen_summary_maybe :: Case_Rule -> String -> Maybe (String, String)
ugen_summary_maybe cr nm = fmap (\u -> (ugen_summary u, u_summary u)) (Db.u_lookup cr nm)

{- | Lookup named 'Ugen' and generate simple summary string.

>>> snd (ugen_summary_err Cs "SinOsc")
"SinOsc kr|ar freq=440 phase=0"

>>> snd (ugen_summary_err Ci "fSinOsc")
"FSinOsc kr|ar freq=440 iphase=0"

>>> snd (ugen_summary_err Sci "sin-osc")
"SinOsc kr|ar freq=440 phase=0"
-}
ugen_summary_err :: Case_Rule -> String -> (String, String)
ugen_summary_err cr = fromMaybe (error "ugen_summary") . ugen_summary_maybe cr

-- | 'unlines' of 'ugen_summary_err' 'Ci'.
ugen_summary_str :: String -> String
ugen_summary_str = (\(p, q) -> unlines [p, "", q]) . ugen_summary_err Ci

{- | 'putStrLn' of 'ugen_summary_str'

> mapM_ ugen_summary_wr (words "out RESONZ EnvGen LocalIn WhiteNoise dwhite LorenzTrig")
-}
ugen_summary_wr :: String -> IO ()
ugen_summary_wr = putStrLn . ugen_summary_str

{- | Generate Hs default parameter string for Ugen.

>>> import Sound.Sc3.Ugen.Db
>>> import Sound.Sc3.Ugen.Db.Record
>>> let Just u = Db.u_lookup_cs "SinOsc" in u_default_param True u
"sinOsc ar 440 0"

>>> let Just u = Db.u_lookup_cs "Resonz" in u_default_param True u
"resonz 0 440 1"

>>> let Just u = Db.u_lookup_cs "Pitch" in u_default_param True u
"pitch 0 440 60 4000 100 16 1 0.01 0.5 1 0"

> import Sound.Sc3.Ugen.Db.Data
> map (u_default_param True . read_meta) sc3_ugen_db
-}
u_default_param :: Bool -> U -> String
u_default_param with_name u =
  let i = map (Math.real_pp 5 . input_default) . ugen_inputs
      r =
        if u_requires_rate u
          then Just (rateAbbrev (ugen_default_rate u))
          else Nothing
      n = if with_name then Just (Rename.fromSc3Name (ugen_name u)) else Nothing
      mcons e l = maybe l (: l) e
  in unwords (mcons n (mcons r (i u)))

{- | Generate control input sequence for U, useful for Ugens with lots of parameters.
     Option to include or not include the Ugen name.

>>> import Sound.Sc3.Ugen.Db
>>> putStrLn (u_control_inputs_pp True (Db.u_lookup_cs_err "Pitch"))
pitch kr (control kr "in" 0) (control kr "initFreq" 440) (control kr "minFreq" 60) (control kr "maxFreq" 4000) (control kr "execFreq" 100) (control kr "maxBinsPerOctave" 16) (control kr "median" 1) (control kr "ampThreshold" 0.01) (control kr "peakThreshold" 0.5) (control kr "downSample" 1) (control kr "clar" 0)
-}
u_control_inputs_pp :: Bool -> U -> String
u_control_inputs_pp with_name u =
  let flt = fromMaybe [] (ugen_filter u)
      f k i =
        if k `elem` flt
          then input_name i
          else concat ["(control kr \"", input_name i, "\" ", Math.real_pp 5 (input_default i), ")"]
  in unwords
      ( (if with_name then Rename.fromSc3Name (ugen_name u) else "")
          : rateAbbrev (ugen_default_rate u)
          : zipWith f [0 ..] (ugen_inputs u)
      )

-- | Generate control param let binding sequence for U.
u_control_param_pp :: U -> String
u_control_param_pp u =
  let flt = fromMaybe [] (ugen_filter u)
      inp = mapMaybe (\(k, i) -> if k `elem` flt then Nothing else Just i) (zip [0 ..] (ugen_inputs u))
      rt = rateAbbrev (ugen_default_rate u)
      in_ = unwords ("in" : Rename.fromSc3Name (ugen_name u) : rt : map input_name inp)
      f k i =
        printf
          "%s %s = control kr \"%s\" %f"
          (if k == 0 then "let" else "   ")
          (input_name i)
          (input_name i)
          (input_default i)
  in unlines (zipWith f [0 :: Int ..] inp ++ [in_])

{- | Generate help notation for Block SuperCollider.
     This gives the name of the Ugen and of its inputs.

>>> let pp nm = let Just u = Db.u_lookup_cs nm in u_block_help_pp u
>>> pp "SinOsc"
"SinOsc(freq, phase)"

>>> pp "WhiteNoise"
"WhiteNoise()"

>>> pp "LocalIn"
"LocalIn(numChan, default)"

>>> pp "CombC"
"CombC(in, maxdelaytime, delaytime, decaytime)"
-}
u_block_help_pp :: U -> String
u_block_help_pp u =
  let nc = if ugen_nc_input u then [I "numChan" 1] else []
      pr = map (\i -> concat [input_name i]) (nc ++ ugen_inputs u)
  in concat [ugen_name u, "(", intercalate ", " pr, ")"]

{- | Generate Smalltalk Ugen exemplar.  Nil param is written new.

>>> let pp nm = let Just u = Db.u_lookup_cs nm in u_smalltalk_pp u
>>> pp "SinOsc"
"SinOsc freq: 440 phase: 0"

>>> pp "LocalIn"
"LocalIn numChan: 1 default: 0"

>>> pp "WhiteNoise"
"WhiteNoise new"
-}
u_smalltalk_pp :: U -> String
u_smalltalk_pp u =
  let nc = if ugen_nc_input u then [I "numChan" 1] else []
      pr = case nc ++ ugen_inputs u of
        [] -> ["new"]
        l -> map (\i -> concat [input_name i, ": ", Math.real_pp 5 (input_default i)]) l
  in unwords (ugen_name u : pr)

{- | Generate SuperCollider Language  Ugen exemplar.
     Option to include keyword parameter notation.
     Nil param is written new.

>>> let pp nm = let Just u = Db.u_lookup_cs nm in u_sclanguage_pp True u
>>> pp "SinOsc"
"SinOsc.ar(freq: 440, phase: 0)"

>>> pp "Pitch"
"Pitch.kr(in: 0, initFreq: 440, minFreq: 60, maxFreq: 4000, execFreq: 100, maxBinsPerOctave: 16, median: 1, ampThreshold: 0.01, peakThreshold: 0.5, downSample: 1, clar: 0)"

>>> pp "WhiteNoise"
"WhiteNoise.ar()"
-}
u_sclanguage_pp :: Bool -> U -> String
u_sclanguage_pp with_kw u =
  let nm = ugen_name u
      rt =
        if "PV_" `isPrefixOf` nm
          then "new"
          else case ugen_default_rate u of
            DemandRate -> "new"
            x -> rateAbbrev x
      nc = if ugen_nc_input u then (if with_kw then "numChannels: 1" else "1") else ""
      pp_def i =
        if with_kw
          then concat [input_name i, ": ", Math.real_pp 5 (input_default i)]
          else Math.real_pp 5 (input_default i)
      arg l = nc : map pp_def l
      f l =
        if null l
          then ""
          else intercalate ", " (filter (not . null) (arg l))
  in concat [nm, ".", rt, "(", f (ugen_inputs u), ")"]

{- | Generate hsc3-rec Ugen exemplar.

>>> let pp nm = let Just u = Db.u_lookup_cs nm in u_hsc3_rec_pp u
>>> pp "SinOsc"
"SinOsc {rate=ar, freq=440, phase=0}"

>>> pp "Pitch"
"Pitch {rate=kr, in=0, initFreq=440, minFreq=60, maxFreq=4000, execFreq=100, maxBinsPerOctave=16, median=1, ampThreshold=0.01, peakThreshold=0.5, downSample=1, clar=0}"

>>> pp "WhiteNoise"
"WhiteNoise {rate=ar, uid='\945'}"
-}
u_hsc3_rec_pp :: U -> String
u_hsc3_rec_pp u =
  let pp_i i = (input_name i, Math.real_pp 5 (input_default i))
      pre =
        catMaybes
          [ if ugen_nc_input u then Just ("nc", "1") else Nothing
          , if ugen_filter u == Nothing then Just ("rate", rateAbbrev (ugen_default_rate u)) else Nothing
          , if ugen_nondet u then Just ("uid", "'α'") else Nothing
          ]
  in case pre ++ map pp_i (ugen_inputs u) of
      [] -> ugen_name u
      l ->
        let pr = map (\(lhs, rhs) -> concat [lhs, "=", rhs]) l
        in printf "%s {%s}" (ugen_name u) (intercalate ", " pr)

{- | Write Ugen parameters (ie. non input fields) as control value Ugen inputs.

>>> ugen_control_inputs_wr "SinOsc"
sinOsc ar (control kr "freq" 440) (control kr "phase" 0)
-}
ugen_control_inputs_wr :: String -> IO ()
ugen_control_inputs_wr nm = putStrLn (u_control_inputs_pp True (Db.u_lookup_ci_err nm))

{- | Write Ugen parameters (ie. non input fields) as let bindings to control values.

>>> ugen_control_param_wr "sinOsc"
let freq = control kr "freq" 440.0
    phase = control kr "phase" 0.0
in sinOsc ar freq phase
<BLANKLINE>

> ugen_control_param_wr "analogVintageDistortion"
-}
ugen_control_param_wr :: String -> IO ()
ugen_control_param_wr nm = putStrLn (u_control_param_pp (Db.u_lookup_ci_err nm))
