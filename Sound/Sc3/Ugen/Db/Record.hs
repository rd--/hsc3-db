-- | UGen DB record definitions.
module Sound.Sc3.Ugen.Db.Record where

import Data.List {- base -}
import Data.Maybe {- base -}

import Sound.Sc3.Common.Rate {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db.Meta as Meta {- hsc3-db -}

-- | UGen input descriptor
data I = I
  { input_name :: String
  , input_default :: Double
  }
  deriving (Eq, Show)

-- | UGen descriptor
data U = U
  { ugen_name :: String
  , ugen_operating_rates :: [Rate]
  , ugen_default_rate :: Rate
  , ugen_inputs :: [I]
  , ugen_outputs :: Maybe Int
  , ugen_summary :: String
  , ugen_std_mce :: Int
  , ugen_nc_input :: Bool
  , ugen_nc_mce :: Maybe Int
  , ugen_filter :: Maybe [Int]
  , ugen_reorder :: Maybe [Int]
  , ugen_enumerations :: Maybe [(Int, String)]
  , ugen_nondet :: Bool
  , ugen_pseudo_inputs :: Maybe [Int]
  , ugen_fixed_rate :: Maybe Rate
  }
  deriving (Eq, Show)

default_u :: U
default_u =
  U
    { ugen_name = ""
    , ugen_operating_rates = []
    , ugen_default_rate = AudioRate
    , ugen_inputs = []
    , ugen_outputs = Nothing
    , ugen_summary = ""
    , ugen_std_mce = 0
    , ugen_nc_input = False
    , ugen_nc_mce = Nothing
    , ugen_filter = Nothing
    , ugen_reorder = Nothing
    , ugen_enumerations = Nothing
    , ugen_nondet = False
    , ugen_pseudo_inputs = Nothing
    , ugen_fixed_rate = Nothing
    }

{- | (nm,rr,rt,inp,outp,dsc)

nm = name,
rr = possible rates,
t = default rate,
np = list of inputs,
outp = output,
sc = description (synopsis)
pu = is PureUGen
-}
type Sc3_U = (String, [Rate], Rate, [I], Int, String, Bool)

remove_nc_inputs :: [I] -> [I]
remove_nc_inputs = let f i = input_name i `notElem` ["numChannels", "numChans"] in filter f

{- | Sc reorders some UGen parameters (ie. the class library and the UGen order inputs differently).
     The Sc3_U record type, generated from the .sc class library, stores the class input order.
     The Meta module stores the permutation required to map from the class order to the UGen order (U type).
-}
reorder_f :: Sc3_U -> [I] -> [Int] -> [I]
reorder_f u inp n =
  if (length inp /= length n) || (sort n /= [0 .. length n - 1])
    then error (show ("reorder_f", "input reorder", u, inp, n))
    else Meta.apply_reorder_forwards n inp

remove_at_indices :: [Int] -> [t] -> [t]
remove_at_indices ix = map snd . filter ((`notElem` ix) . fst) . zip [0 ..]

{- | Read Meta for Sc3_U and derive U.
     This reorders inputs as required, and deletes pseudo-inputs.
-}
read_meta :: Sc3_U -> U
read_meta sc3_u =
  let (nm, rr, rt, inp, outp, dsc, _) = sc3_u
      m = Meta.meta_data_lookup_def nm
      nc = Meta.m_nc_input m
      inp' = if nc then remove_nc_inputs inp else inp
      srt = Meta.m_input_reorder m
      rmps x = case Meta.m_pseudo_inputs m of
        Nothing -> x
        Just i -> remove_at_indices i x
      inp'' =
        rmps
          ( case srt of
              Nothing -> inp'
              Just n -> reorder_f sc3_u inp' n
          )
      fx = case Meta.m_fixed_rate m of
        Just r -> Just r
        Nothing -> if rr == [DemandRate] then Just DemandRate else Nothing
      mir = if Meta.m_i_rate m then [InitialisationRate] else []
      rr' = nub (sort (mir ++ rr ++ maybe [] return fx))
  in U
      { ugen_name = nm
      , ugen_operating_rates = rr'
      , ugen_default_rate = rt
      , ugen_inputs = inp''
      , ugen_outputs =
          if Meta.m_has_var_inputs m
            then Nothing
            else case Meta.m_n_outputs m of
              Just outp' -> Just outp'
              Nothing -> Just outp
      , ugen_summary = dsc
      , ugen_std_mce = Meta.m_std_mce m
      , ugen_nc_input = nc
      , ugen_nc_mce = Meta.m_nc_mce m
      , ugen_filter = Meta.m_filter m
      , ugen_reorder = srt
      , ugen_enumerations = Meta.m_enumerations m
      , ugen_nondet = Meta.m_nondet m || rr == [DemandRate]
      , ugen_pseudo_inputs = Meta.m_pseudo_inputs m
      , ugen_fixed_rate = fx
      }

input_enumeration :: U -> Int -> Maybe String
input_enumeration u ix =
  case ugen_enumerations u of
    Just e -> lookup ix e
    Nothing -> Nothing

-- | Infinite default value
inf :: Double
inf = 10 ** 8

-- | Enumeration for @Onsets@ UGen @odftype@ input.
rcomplex :: Double
rcomplex = 3

-- | 'true' is @1@.
true :: Double
true = 1

-- | 'false' is @0@.
false :: Double
false = 0

{- | True if the unit generator can operate at multiple rates, in which
     case the record will require a rate field.
     ie. is U neither a filter nor a fixed-rate UGen.

>>> import Sound.Sc3.Ugen.Db
>>> import Sound.Sc3.Ugen.Db.Record
>>> map (u_requires_rate . u_lookup_cs_err) ["SinOsc","Pitch","MouseX","RLPF"]
[True,False,True,False]
-}
u_requires_rate :: U -> Bool
u_requires_rate u = isNothing (ugen_filter u) && isNothing (ugen_fixed_rate u)

-- | 'length' of 'ugen_inputs'
u_num_inputs :: U -> Int
u_num_inputs = length . ugen_inputs

-- | Alternate view of 'ugen_nc_input' and 'ugen_nc_mce'.
u_fixed_outputs :: U -> Maybe Int
u_fixed_outputs u =
  case (ugen_nc_input u, ugen_nc_mce u) of
    (False, Nothing) -> ugen_outputs u
    _ -> Nothing

-- | Is 'DemandRate' the only allowed 'Rate'?
u_is_demand_rate :: U -> Bool
u_is_demand_rate = (== [DemandRate]) . ugen_operating_rates

{- | List of input names.

>>> import Sound.Sc3.Ugen.Db
>>> import Sound.Sc3.Ugen.Db.Record
>>> fmap u_input_names (u_lookup_cs "SinOsc")
Just ["freq","phase"]

>>> fmap u_input_names (u_lookup_cs "BufRd")
Just ["bufnum","phase","loop","interpolation"]

>>> fmap u_input_names (u_lookup_cs "Dseq")
Just ["repeats","list"]

>>> fmap u_input_names (u_lookup_cs "Gate")
Just ["in","trig"]
-}
u_input_names :: U -> [String]
u_input_names = map input_name . ugen_inputs

-- | Is U a filter?
u_is_filter :: U -> Bool
u_is_filter = isJust . ugen_filter

-- | Is U a filter at input zero?
u_is_filter_at_0 :: U -> Bool
u_is_filter_at_0 u =
  case ugen_filter u of
    Just x -> 0 `elem` x
    _ -> False

-- | Is U a filter with k inputs indicated?
u_is_nary_filter :: Int -> U -> Bool
u_is_nary_filter k u =
  case ugen_filter u of
    Just x -> length x == k
    _ -> False

-- | Is U a filter with one input indicated?
u_is_unary_filter :: U -> Bool
u_is_unary_filter = u_is_nary_filter 1

{- | Index (0 based) of index to filter input at.
     This is the index of the first filter input.
-}
u_filter_ix :: U -> Int
u_filter_ix = head . fromJust . ugen_filter

-- | Is the input 'I' mce collapsed at 'U'.
i_is_mce :: U -> I -> Bool
i_is_mce u i =
  let take_right :: Int -> [a] -> [a]
      take_right k = reverse . take k . reverse
      n = ugen_std_mce u
  in n > 0 && i `elem` take_right n (ugen_inputs u)

-- | Does U have an Mce rule, ie. does it not partake in ordinary array expansion.
u_has_mce_rule :: U -> Bool
u_has_mce_rule u = isJust (ugen_nc_mce u) || (ugen_std_mce u > 0)
