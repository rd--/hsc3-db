-- | Renaming functions for Ugen descriptions.
module Sound.Sc3.Ugen.Db.Rename where

import Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Sound.Sc3.Ugen.Name as Name {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db.Meta as Meta {- hsc3-db -}
import Sound.Sc3.Ugen.Db.Record {- hsc3-db -}

-- * Plain

{- | Rename parameters that conflict with /Haskell/ or /Lisp/ keywords or
'Prelude' functions, or with Sc3.Ugens or which have otherwise unwieldy names.

>>> map rename_input ["in","id"]
["input","id_"]
-}
rename_input :: String -> String
rename_input p =
  let input_l = words "in channelsArray"
      keyword_l = words "data default in type pattern"
      prelude_l = words "div drop exp floor id init length min max mod round sum tail"
      hsc3_l = words "rate control label envelope gate trig loudness decay"
      lisp_l = words "list"
  in if p `elem` input_l
      then "input"
      else
        if p `elem` concat [keyword_l, prelude_l, hsc3_l, lisp_l]
          then p ++ "_"
          else p

-- | Case insensitive 'String' '=='.
ci_eq :: String -> String -> Bool
ci_eq p q = let f = map toLower in f p == f q

-- | If the input name is the same as the ugen name, rename the input with trailing underscore.
rename_eq_input :: String -> String -> String
rename_eq_input u =
  let f x = if x `ci_eq` u then x ++ "_" else x
  in rename_input . f

-- | Names of Sc3 operators that conflict with Rnrs scheme names.
scheme_names :: [String]
scheme_names =
  let uop = "abs cos exp floor log not sin sqrt tan"
      binop = "gcd lcm max min mod round"
  in words (unlines [uop, binop])

-- | Prefix reserved names with 'u:'.  This is required if lower cases names are used for operators such as abs, cos, sin &etc.
scheme_rename :: String -> String
scheme_rename nm = if nm `elem` scheme_names then "u:" ++ nm else nm

-- * Record

-- | 'rename_eq_input' at 'I' of 'U'.
i_rename :: U -> I -> I
i_rename u i = i {input_name = rename_eq_input (ugen_name u) (input_name i)}

-- | 'i_rename' at 'U'.
u_rename :: U -> U
u_rename u =
  let i' = map (i_rename u) (ugen_inputs u)
  in u {ugen_inputs = i'}

-- | Rename input if it's listed (case insensitive).
i_rename_db :: [String] -> I -> I
i_rename_db uu_nm i =
  let n = input_name i
      n' = case find (ci_eq n) uu_nm of
        Just _ -> n ++ "_"
        Nothing -> n
  in i {input_name = rename_input n'}

-- | Variant that renames inputs to avoid name collisions with any other Ugen.
u_rename_db :: [U] -> U -> U
u_rename_db uu u =
  let uu_nm = map ugen_name uu
      i' = map (i_rename_db uu_nm) (ugen_inputs u)
  in u {ugen_inputs = i'}

-- | List of renamed inputs at 'U'.
u_renamed_inputs :: U -> [String]
u_renamed_inputs u = map (rename_input . input_name) (ugen_inputs u)

-- * Ugen names

{- | Convert from @Sc3@ Ugen name to @hsc3@ name.
     This is also the form for unary filter method names.

>>> fromSc3Name "SinOsc"
"sinOsc"

>>> map fromSc3Name ["LFSaw","PV_Copy","FBSineN"]
["lfSaw","pv_Copy","fbSineN"]

>>> map fromSc3Name ["BPF","FFT","TPV"]
["bpf","fft","tpv"]

>>> map fromSc3Name (words "HPZ1 RLPF")
["hpz1","rlpf"]

>>> fromSc3Name "In"
"in'"

> import Sound.Sc3.Common.Math.Operator
> mapM_ (\(x,_) -> putStrLn (unwords [fromSc3Name x,x])) sc3_unary_op_tbl
> mapM_ (\(x,_) -> putStrLn (unwords [fromSc3Name x,x])) sc3_binary_op_tbl
-}
fromSc3Name :: String -> String
fromSc3Name nm =
  let m = Meta.meta_data_lookup_def nm
  in fromMaybe (Name.sc3_name_to_hs_name nm) (Meta.m_hs_name m)

{- | Renaming table

>>> nub (map (head . snd) sc3_ugen_renaming_table)
"ABDFGHILMPRSTV"
-}
sc3_ugen_renaming_table :: [(String, String)]
sc3_ugen_renaming_table =
  map
    (\txt -> case words txt of [lhs, rhs] -> (lhs, rhs); _ -> error "sc3_renaming_table")
    [ "APF Apf"
    , "BPF Bpf"
    , "BPZ2 Bpz2"
    , "BPeakEQ BPeakEq"
    , "BRF Brf"
    , "BRZ2 Brz2"
    , "DC Dc"
    , "FBSineC FbSineC"
    , "FBSineL FbSineL"
    , "FFT Fft"
    , "FOS Fos"
    , "GrainFM GrainFm"
    , "HPF Hpf"
    , "HPF Hpf"
    , "HPZ1 Hpz1"
    , "HPZ2 Hpz2"
    , "IFFT Ifft"
    , "LFClipNoise LfClipNoise"
    , "LFCub LfCub"
    , "LFDNoise1 LfdNoise1"
    , "LFDNoise3 LfdNoise3"
    , "LFGauss LfGauss"
    , "LFNoise0 LfNoise0"
    , "LFNoise1 LfNoise1"
    , "LFNoise2 LfNoise2"
    , "LFPar LfPar"
    , "LFPulse LfPulse"
    , "LFSaw LfSaw"
    , "LFTri LfTri"
    , "LPF Lpf"
    , "LPZ1 Lpz1"
    , "LPZ2 Lpz2"
    , "Lag3UD Lag3Ud"
    , "LagUD LagUd"
    , "LeakDC LeakDc"
    , "MidEQ MidEq"
    , "MoogFF MoogFf"
    , "PMOsc PmOsc"
    , "RHPF Rhpf"
    , "RLPF Rlpf"
    , "SOS Sos"
    , "SetResetFF SetResetFf"
    , "SinOscFB SinOscFb"
    , "TIRand TiRand"
    , "ToggleFF ToggleFf"
    , -- PV_
      "PV_Diffuser PvDiffuser"
    , "PV_RandComb PvRandComb"
    , -- External
      "DFM1 Dfm1"
    , "DWGPluckedStiff DwgPluckedStiff"
    , "FM7 Fm7"
    , "LFBrownNoise1 LfBrownNoise1"
    , "MoogVCF MoogVcf"
    , "MZPokey MzPokey"
    , "RCD Rcd"
    , "SawDPW SawDpw"
    , "SCM Scm"
    , "VBJonVerb VbJonVerb"
    , "VOSIM Vosim"
    ]

{- | Rename Ugen according to 'sc3_ugen_renaming_table'.

>>> sc3_ugen_rename "LFSaw"
"LfSaw"
-}
sc3_ugen_rename :: String -> String
sc3_ugen_rename name =
  case lookup name sc3_ugen_renaming_table of
    Just answer -> answer
    Nothing -> name

{- | Lookup initial Ugen name according to 'sc3_ugen_renaming_table'.

>>> sc3_ugen_initial_name "LfSaw"
"LFSaw"
-}
sc3_ugen_initial_name :: String -> String
sc3_ugen_initial_name name =
  case lookup name (map (\(p, q) -> (q, p)) sc3_ugen_renaming_table) of
    Just answer -> answer
    Nothing -> name
