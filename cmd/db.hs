import System.Environment {- base -}

import qualified Sound.Sc3.Ugen.Db.Rename as Name {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}

import qualified Sound.Sc3.Ugen.Db.Pp.Graph as Pp {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Record {- hsc3-db -}

usage :: [String]
usage =
  [ "hsc3-db cmd [arg]"
  , "  dmenu {cat | ugen} {core | external}"
  , "  list {all | core | external} {hs | sc}"
  , "  scsyndef-to-fs [scyndef] [fs]"
  , "  xmenu {all | core | external}"
  ]

main :: IO ()
main = do
  arg <- getArgs
  let putStrLn_not_nil x = if null x then return () else putStrLn x
      db_typ typ = case typ of
        "all" -> Db.ugen_db
        "core" -> Db.ugen_db_core
        "ext" -> Db.ugen_db_ext
        _ -> error (unlines usage)
      nm_for for = if for == "hs" then Name.fromSc3Name else id
  case arg of
    ["dmenu", "cat", "core"] -> Db.ugen_cat_dmenu_core >>= putStrLn_not_nil
    ["dmenu", "cat", "external"] -> Db.ugen_cat_dmenu_ext >>= putStrLn_not_nil
    ["dmenu", "ugen", typ] -> Db.ugen_name_dmenu (db_typ typ) >>= putStrLn_not_nil
    ["list", "ugen", typ, for] -> putStrLn (unwords (map (nm_for for . Record.ugen_name) (db_typ typ)))
    ["scsyndef-to-fs"] -> Pp.scsyndef_to_fs "/tmp" "/dev/stdin" "/dev/stdout"
    ["scsyndef-to-fs", sy_fn] -> Pp.scsyndef_to_fs "/tmp" sy_fn "/dev/stdout"
    ["scsyndef-to-fs", sy_fn, fs_fn] -> Pp.scsyndef_to_fs "/tmp" sy_fn fs_fn
    ["scsyndef-to-sc", sy_fn, sc_fn] -> Pp.scsyndef_to_sc "/tmp" sy_fn sc_fn
    ["scsyndef-to-st", sy_fn, st_fn] -> Pp.scsyndef_to_st "/tmp" sy_fn st_fn
    ["xmenu", "core"] -> Db.ugen_cat_xmenu_core >>= putStrLn
    ["xmenu", "external"] -> Db.ugen_cat_xmenu_ext >>= putStrLn
    _ -> putStrLn (unlines usage)
